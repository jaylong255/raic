﻿jQuery(document).ready(function($) {

/**********************************************************************
 Load a random background image on page load
 Reference: http://briancray.com/posts/simple-image-randomizer-jquery
 Added by Freeform Solutions - S.Gray - Sept. 17, 2014
 **********************************************************************/
 
 // Define the images folder (include trailing slash)
 var imgfold = "/sites/all/themes/raic_2014/images/raic-bg/";
 
 // Define the images (do not include path)
 var images = ['bg_01.jpg', 'bg_02.jpg', 'bg_03.jpg', 'bg_04.jpg', 'bg_05.jpg', 'bg_06.jpg', 'bg_07.jpg', 'bg_08.jpg', 'bg_09.jpg', 'bg_10.jpg', 'bg_11.jpg', 'bg_12.jpg', 'bg_13.jpg'];
 
 // Set an image as the background
 $("body").css({"background-image": "url(" + imgfold + images[Math.floor(Math.random() * images.length)] + ")"});
 
});