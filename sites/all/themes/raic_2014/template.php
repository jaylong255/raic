<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 
 /**
 * Implements template_preprocess_views_slideshow_controls_text_previous().
 */
 
function raic_2014_preprocess_page(&$variables, $hook) {  // Add theme suggestion for all content types
    if (isset($variables['node'])) {
        if ($variables['node']->type != '') {
            $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
        }
    }
}

function raic_2014_preprocess_views_slideshow_controls_text_previous(&$vars) {
  $vars['previous_text'] = '<';
}

/**
 * Implements template_preprocess_views_slideshow_controls_text_pause().
 */
function raic_2014_preprocess_views_slideshow_controls_text_pause(&$vars) {
  $vars['start_text'] = ' ';
}

/**
 * Implements template_preprocess_views_slideshow_controls_text_next().
 */
function raic_2014_preprocess_views_slideshow_controls_text_next(&$vars) {
  $vars['next_text'] = '>';
}

/**
 * Custom theme function for the login/register link.
 */
function raic_2014_theme_lt_login_link($variables) {
  // Only display register text if registration is allowed.
  if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
    return t('Log in/Register');
  }
  else {
    return t('Member Login');
  }
}
