﻿jQuery(document).ready(function($) {

/**********************************************************************
 Load a random background image on page load
 Reference: http://briancray.com/posts/simple-image-randomizer-jquery
 Added by Freeform Solutions - S.Gray - Sept. 17, 2014
 **********************************************************************/
 
 // Define the images folder (include trailing slash)
 var imgfold = "/sites/all/themes/raic_festival/images/raic-bg/";
 
 // Define the images (do not include path)
 var images = ['bg_01.jpg'];
 
 // Set an image as the background
 $("body").css({"background-image": "url(" + imgfold + images[Math.floor(Math.random() * images.length)] + ")"});
 
});