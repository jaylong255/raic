<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<style type="text/css">

@font-face {
	font-family: 'Open Sans-Italic';
	src: url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Italic.ttf'),
	url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Italic.woff') format('woff'), url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Italic.eot') format('oet');
}
@font-face {
	font-family: 'Open Sans-Light';
	src: url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Light.ttf') format('truetype'),
	url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Light.woff') format('woff'), url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Light.eot') format('oet');
}
@font-face {
	font-family: 'Open Sans-LightItalic';
	src: url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-LightItalic.ttf'),
	url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-LightItalic.woff') format('woff'), url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-LightItalic.eot') format('oet');
}
@font-face {
	font-family: 'Open Sans-Regular';
	src: url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Regular.ttf'),
	url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Regular.woff') format('woff'), url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Regular.eot') format('oet');
}
@font-face {
	font-family: 'Open Sans-Semibold';
	src: url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Semibold.ttf'),
	url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Semibold.woff') format('woff'), url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Semibold.eot') format('oet');
}
@font-face {
	font-family: 'Open Sans-Bold';
	src: url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Bold.ttf'),
	url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Bold.woff') format('woff'), url('https://raic.org/sites/all/themes/raic_2014/fonts/OpenSans-Bold.eot') format('oet');
}

body {
	font-family: "Open Sans";
	color: #333c42;
	margin-top: 50px;
	margin-bottom: 50px;
	background-color: #F7F9FB;
	background: no-repeat;
}
h1 {
	font-family: "Open Sans", bold, "OpenSans Bold", 'Open Sans-Bold';
	font-size: 36px;
	line-height: 48px;
	color: #F04E37;
	margin-top: 10px;
	margin-bottom: 10px;
}
h2 {
	font-family: "Open Sans", bold, "OpenSans Bold", 'Open Sans-Bold';
	font-size: 22px;
	font-weight: bolder;
	line-height: 28px;
	margin-top: 5px;
	margin-bottom: 5px;
}
p {
	font-family: "Open Sans", 'Open Sans-Regular';
	font-size: 14px;
	line-height: 20px;
	margin-bottom: 16px;
	margin-top: 16px;
}
ul {
	font-family: "Open Sans", 'Open Sans-Regular';
	font-size: 14px;
	line-height: 20px;
	margin-top: 20px;
	margin-bottom: 20px;
}
li {
	margin-bottom: 8px;
	padding-left: 10px;
}
.listed-link {
	font-family: "Open Sans";
	font-weight: bold;
	list-style-image: url(https://raic.org/notices/templates/common-images/arrow2.gif);
}
.content {
	padding-top: 20px;
	padding-right: 40px;
	padding-bottom: 20px;
	padding-left: 40px;
}
.bar-top {
	font-family: "Open Sans";
	font-size: 10px;
	line-height: 13px;
	color: #FFF;
	text-align: right;
	vertical-align: middle;
	padding-right: 15px;
	padding-left: 40px;
}
#bar-top a {
	color: #ffffff;
	text-decoration: none;
}
#bar-top a:hover {
	color: #FFFFFF;
	text-decoration: underline;
}
#bar-top a:active {
	color: #ffffff;
}

.bar-bottom {
	font-family: "Open Sans", 'Open Sans-Regular';
	font-size: 14px;
	color: #FFF;
	text-align: right;
	vertical-align: middle;
	padding-right: 15px;
}
#bar-bottom a {
	color: #ffffff;
	text-decoration: none;
}
#bar-bottom a:hover {
	color: #FFFFFF;
	text-decoration: underline;
}
#bar-bottom a:active {
	color: #ffffff;
}

.social {
	text-align: left;
	padding-left: 40px;
}
a:link {
	color: #F04E37;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #F04E37;
}
a:hover {
	text-decoration: underline;
	color: #F04E37;
}
a:active {
	text-decoration: none;
	color: #F04E37;
}
</style>

</head>

<body bgcolor="#F7F9FB">
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td height="35" colspan="5" bgcolor="#F04E37" class="bar-top" id="bar-top"></td>
  </tr>
  <tr>
    <td colspan="5"><a href="https://raic.org" target="_blank" border="0"><img src="https://raic.org/notices/templates/common-images/masthead_e.jpg" width="600" height="140" /></a></td>
  </tr>
  <tr>
    <td colspan="5" class="content">
    <h1 class="title" id="page-title"><?php print $title; ?></h1>

      <p><?php print $content; ?></p>
        </td>
  </tr>
  <tr>
    <td width="146" height="40" bgcolor="#333C42" class="social"><a href="https://twitter.com/#!/arch_canada" target="_blank"><img src="https://raic.org/notices/templates/common-images/twitter.jpg" width="24" height="24" border="0"/></a>&nbsp;&nbsp;&nbsp;<a href="http://www.linkedin.com/e/-afgo7g-h4qzyz2o-6e/vgh/4534752/eml-grp-sub/?hs=false&amp;tok=1GmVVGxfxjr5k1" target="_blank"><img src="https://raic.org/notices/templates/common-images/linkedin.jpg" width="24" height="24" border="0"/></a>&nbsp;&nbsp;&nbsp;<a href="http://www.facebook.com/pages/Architecture-Canada-RAIC-IRAC/414616818595397" target="_blank"><img src="https://raic.org/notices/templates/common-images/facebook.jpg" width="24" height="24" border="0"/></a></td>
    <td width="23" bgcolor="#333C42" class="bar-bottom" id="bar-bottom">&nbsp;</td>
    <td width="145" bgcolor="#333C42" class="bar-bottom" id="bar-bottom">&nbsp;</td>
    <td width="181" bgcolor="#333C42" class="bar-bottom" id="bar-bottom">&nbsp;</td>
    <td width="105" bgcolor="#333C42" class="bar-bottom" id="bar-bottom">&nbsp;</td>
  </tr>
</table>
</body>
</html>
