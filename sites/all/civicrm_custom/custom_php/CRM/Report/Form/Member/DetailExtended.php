<?php

/*
 +--------------------------------------------------------------------+
 | CiviCRM version 3.4                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2011                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*/

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2011
 * $Id$
 *
 */

require_once 'CRM/Report/Form.php';
require_once 'CRM/Member/PseudoConstant.php';

class CRM_Report_Form_Member_DetailExtended extends CRM_Report_Form {

  protected $_addressField = FALSE;
  protected $_emailField   = FALSE;
  protected $_phoneField   = FALSE;
  protected $_employerField   = FALSE;

  protected $_summary      = NULL;

  protected $_customGroupExtends = array('Membership', 'Contact', 'Individual');
  protected $_customGroupGroupBy = FALSE;
  protected $_customDoNotAlter = array('civicrm_value_membership_approval_12_custom_42');

  protected $_genders     = NULL;
  protected $_prefixes    = NULL;
  protected $_suffixes    = NULL;
  protected $_langs       = NULL;
  protected $_booleans    = NULL;

  //protected $_autoIncludeIndexedFieldsAsOrderBys = array( 'Membership', 'Contact' );

  /**
   * [__construct description]
   */
  public function __construct() {

    $this->_genders   = CRM_Core_PseudoConstant::get('CRM_Contact_DAO_Contact', 'gender_id');
    $this->_prefixes  = CRM_Core_PseudoConstant::get('CRM_Contact_DAO_Contact', 'prefix_id');
    $this->_suffixes  = CRM_Core_PseudoConstant::get('CRM_Contact_DAO_Contact', 'suffix_id');
    $this->_langs     = CRM_Core_PseudoConstant::get('CRM_Contact_DAO_Contact', 'preferred_language');
    //CRM_Core_I18n_PseudoConstant::languages();
    $this->_booleans  = array(1 => ts('Yes'), 0 => ts('No'));

    self::validRelationships();

    $this->_columns = array(
      'civicrm_contact' =>
        array(
          'dao' => 'CRM_Contact_DAO_Contact',
          'fields' => array(
            'sort_name' => array(
              'title' => ts('Contact Name'),
              'required'   => TRUE,
              'default'    => TRUE,
              'no_repeat'  => TRUE,
            ),
            'id' => array(
              'no_display' => TRUE,
              'required'   => TRUE,
            ),
            'cid' => array(
              'title'      => ts('Contact ID'),
              'name'       => 'id',
            ),
            'prefix_id' => array(
              'title'      => ts('Personal Prefix'),
            ),
            'first_name' => array(
              'title'      => ts('First Name'),
            ),
            'middle_name' => array(
              'title' => ts('Middle Name'),
            ),
            'last_name' => array(
              'title'      => ts('Last Name'),
            ),
            'suffix_id' => array(
              'title'      => ts('Personal Suffix'),
            ),
            'nick_name' => array(
              'title'    => ts('Nick Name'),
            ),
            'job_title' => array(
              'title'      => ts('Job Title'),
            ),
            'preferred_language' => array(
              'title'      => ts('Preferred Language'),
            ),
            'gender_id' => array(
              'title'      => ts('Gender'),
            ),
            'birth_date' => array(
              'title'      => ts('Date of birth'),
            ),
            'is_deceased' => array(
              'title'      => ts('Contact is deceased'),
            ),
            'deceased_date' => array(
              'title'      => ts('Deceased date'),
            ),
          ),
          'filters'  => array(
            'sort_name'     => array(
              'title'    => ts('Contact Name'),
              'operator' => 'like',
            ),
            'first_name'     => array(
              'title'    => ts('First Name'),
              'operator' => 'like',
            ),
            'middle_name'     => array(
              'title'    => ts('Middle Name'),
              'operator' => 'like',
            ),
            'last_name'     => array(
              'title'    => ts('Last Name'),
              'operator' => 'like',
            ),
            'job_title'  => array(
              'title'    => ts('Job Title'),
              'operator' => 'like',
            ),
            'preferred_language'     => array(
              'title'    => ts('Preferred Language'),
              'type'          => CRM_Utils_Type::T_STRING,
              'operatorType'  => CRM_Report_Form::OP_MULTISELECT,
              'options'       => $this->_langs,
            ),
            'gender_id'     => array(
              'title'    => ts('Gender'),
              'type'          => CRM_Utils_Type::T_INT,
              'operatorType'  => CRM_Report_Form::OP_MULTISELECT,
              'options'       => $this->_genders,
            ),
            'birth_date'    => array('operatorType' => CRM_Report_Form::OP_DATE),
            'is_deceased'     => array(
              'title'    => ts('Contact is deceased'),
              'type'          => CRM_Utils_Type::T_INT,
              'operatorType'  => CRM_Report_Form::OP_MULTISELECT,
              'options'       => $this->_booleans,
            ),
            'has_duplicate_mechs'   => array(
              'name'    => 'dup_mechs',
              'title'        => ts('Duplicate Directory Contact Info'),
              'type'         => CRM_Utils_Type::T_INT,
              'operatorType' => CRM_Report_Form::OP_SELECT,
              'options'      => array('' => ts('- select -'), 1 => ts('Yes')),
            ),
          ),
          'grouping' => 'contact-fields',
        ),
      'civicrm_contact_organization' => array(
        'dao'           => 'CRM_Contact_DAO_Contact',
        'fields'        => array(
          'organization_name' => array(
            'title'    => ts('Employer Name'),
          ),
          'id' => array(
            'no_display' => TRUE,
            'required'   => TRUE,
          ),
        ),
        'filters' => array(
          'organization_name' => array(
            'title'      => ts('Organization Name'),
          ),
        ),
        'grouping' => 'contact-fields',
      ),
      'civicrm_relationship' => array(
        'dao'           => 'CRM_Contact_DAO_Relationship',
        'grouping' => 'contact-fields',
      ),
      'civicrm_membership' => array(
        'dao'       => 'CRM_Member_DAO_Membership',
        'fields'    => array(
          'membership_type_id'    => array(
            'title'     => 'Membership Type',
            'required'  => TRUE,
          ),
          //'no_repeat' => TRUE ),
          'years_passed'          => array(
            'title'   => 'Years Passed (For Rule)',
            'default' => TRUE,
            'dbAlias' => "''",
            'type'         => CRM_Utils_Type::T_DATE,
          ),
          'next_membership_type'  => array(
            'title'     => 'Next Membership Type',
            'default' => TRUE,
            'dbAlias' => 'membership_type_id',
            'type'         => CRM_Utils_Type::T_STRING,
          ),
          'membership_start_date' => array(
            'title'     => ts('Start Date'),
            'default'   => TRUE,
          ),
          'membership_end_date'   => array(
            'title'     => ts('End Date'),
            'default'   => TRUE,
          ),
          'join_date'             => array(
            'title'     => ts('Join Date'),
            'default'   => TRUE,
          ),
          'source'                => array(
            'title' => 'Source',
          ),
        ),
        'filters' => array(
          'join_date'    => array(
            'operatorType'  => CRM_Report_Form::OP_DATE,
          ),
          'membership_start_date' => array(
            'operatorType'  => CRM_Report_Form::OP_DATE,
          ),
          'membership_end_date'   => array(
            'operatorType'  => CRM_Report_Form::OP_DATE,
          ),
          'owner_membership_id'  => array(
            'title'         => ts('Membership Owner ID'),
            'operatorType'  => CRM_Report_Form::OP_INT,
          ),
          'tid'          => array(
            'name'          => 'membership_type_id',
            'title'         => ts('Membership Types'),
            'type'          => CRM_Utils_Type::T_INT,
            'operatorType'  => CRM_Report_Form::OP_MULTISELECT,
            'options'       => CRM_Member_PseudoConstant::membershipType(),
          ),
        ),
        'grouping' => 'member-fields',
      ),
      'civicrm_membership_status' => array(
        'dao'      => 'CRM_Member_DAO_MembershipStatus',
        'alias'    => 'mem_status',
        'fields'   => array(
          'name'   => array(
            'title'   => ts('Status'),
            'default' => TRUE,
          ),
        ),
        'filters'  => array(
          'sid' => array(
            'name'         => 'id',
            'title'        => ts('Status'),
            'type'         => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options'      => CRM_Member_PseudoConstant::membershipStatus(NULL, NULL, 'label'),
          ),
        ),
        'grouping' => 'member-fields',
      ),
      'civicrm_membership_type' => array(
        'dao'      => 'CRM_Member_DAO_MembershipType',
        'alias'    => 'mem_type',
        'fields'   => array(
          'minimum_fee'        => array(
            'title'     => 'Minimum Fee (Current)',
            'type'      => CRM_Utils_Type::T_MONEY,
          ),
          'tax_payable'        => array(
            'title'     => 'Tax Payable (Current)',
            'type'      => CRM_Utils_Type::T_MONEY,
            'dbAlias'   => '0.00',
          ),
          'total_payable'      => array(
            'title'     => 'Total Payable (Current)',
            'type'      => CRM_Utils_Type::T_MONEY,
            'dbAlias'   => '0.00',
          ),
        ),
        'filters'  => array(
          'mtid' => array(
            'name'         => 'id',
            'title'        => ts('Membership Type'),
            'type'         => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options'      => CRM_Member_PseudoConstant::membershipType(),
          ),
        ),
        'grouping' => 'member-fields',
      ),
      /*
       * Note: To display Minimum Fee and Taxes for next membership type the following fields must be enabled:
       *  - Country and State/Province on the contact
       *  - Years Passed for rule and next membership type
       */
      'civicrm_next_membership_type' => array(
        'dao'      => 'CRM_Member_DAO_MembershipType',
        'alias'    => 'next_mem_type',
        'fields'   => array(
          'next_minimum_fee'        => array(
            'title'     => 'Minimum Fee (Upcoming)',
            'type'      => CRM_Utils_Type::T_MONEY,
            'dbAlias'   => '0.00',
          ),
          'next_tax_payable'        => array(
            'title'     => 'Tax Payable (Upcoming)',
            'type'      => CRM_Utils_Type::T_MONEY,
            'dbAlias'   => '0.00',
          ),
          'next_total_payable'      => array(
            'title'     => 'Total Payable (Upcoming)',
            'type'      => CRM_Utils_Type::T_MONEY,
            'dbAlias'   => '0.00',
          ),
        ),
        'filters'  => array(
          'mtid' => array(
            'name'         => 'id',
            'title'        => ts('Membership Type'),
            'type'         => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options'      => CRM_Member_PseudoConstant::membershipType(),
          ),
        ),
        'grouping' => 'member-fields',
      ),
      'civicrm_address' => array(
        'dao'      => 'CRM_Core_DAO_Address',
        'fields'   => array(
          'street_address'    => NULL,
          'supplemental_address_1' => array(
            'title'      => ts("Addt'l Address 1"),
          ),
          'supplemental_address_2' => array(
            'title'      => ts("Addt'l Address 2"),
          ),
          'city'              => NULL,
          'postal_code'       => NULL,
          'state_province_id' => array(
            'title'      => ts('State/Province'),
          ),
          'country_id'        => array(
            'title'      => ts('Country'),
          ),
        ),
        'filters'  => array(
          'country_id' => array(
            'title'        => ts('Country'),
            'type'         => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options'      => CRM_Member_PseudoConstant::country(FALSE, TRUE),
          ),
          'state_province_id' => array(
            'title'        => ts('State/Province'),
            'type'         => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options'      => CRM_Member_PseudoConstant::stateProvince(FALSE, TRUE),
          ),
        ),
        'grouping' => 'address-fields',
      ),
      'civicrm_email' => array(
        'dao'    => 'CRM_Core_DAO_Email',
        'fields' => array(
          'email' => NULL,
        ),
        'grouping' => 'contact-fields',
      ),
      'civicrm_phone' => array(
        'dao'    => 'CRM_Core_DAO_Phone',
        'fields' => array(
          'phone' => NULL,
        ),
        'grouping' => 'address-fields',
      ),
    );
    $this->_groupFilter = TRUE;
    $this->_tagFilter = TRUE;
    parent::__construct();
    //CRM_Core_Error::debug('columns-end-contruct', $this->_columns);
  }

  function preProcess() {
    parent::preProcess();
  }

  function select() {
    $select = $this->_columnHeaders = array();

    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('fields', $table)) {
        foreach ($table['fields'] as $fieldName => $field) {
          if (CRM_Utils_Array::value('required', $field) ||
            CRM_Utils_Array::value($fieldName, $this->_params['fields'])) {
            if ($tableName == 'civicrm_address') {
              $this->_addressField = TRUE;
            }
            elseif ($tableName == 'civicrm_email') {
              $this->_emailField = TRUE;
            }
            elseif ($tableName == 'civicrm_phone') {
              $this->_phoneField = TRUE;
            }
            elseif ($tableName == 'civicrm_contact_organization') {
              $this->_employerField = TRUE;
            }
            if ($tableName == 'civicrm_membership' && $fieldName == 'next_membership_type') {
              $select[] = "{$field['dbAlias']} as {$tableName}_{$fieldName}";
            }
            else {
              $select[] = "{$field['dbAlias']} as {$tableName}_{$fieldName}";
            }
            $this->_columnHeaders["{$tableName}_{$fieldName}"]['title'] = $field['title'];
            $this->_columnHeaders["{$tableName}_{$fieldName}"]['type']  = CRM_Utils_Array::value('type', $field);
          }
        }
      }
    }
    $this->_select = "SELECT " . implode(', ', $select) . " ";
  }

  function from() {
    $this->_from = NULL;

    $this->_from = "
     FROM
     civicrm_contact {$this->_aliases['civicrm_contact']}
     {$this->_aclFrom}
           LEFT JOIN civicrm_membership {$this->_aliases['civicrm_membership']}
                      ON {$this->_aliases['civicrm_contact']}.id =
                         {$this->_aliases['civicrm_membership']}.contact_id
           LEFT  JOIN civicrm_membership_status {$this->_aliases['civicrm_membership_status']}
                      ON {$this->_aliases['civicrm_membership_status']}.id =
                         {$this->_aliases['civicrm_membership']}.status_id
           LEFT  JOIN civicrm_membership_type {$this->_aliases['civicrm_membership_type']}
                      ON {$this->_aliases['civicrm_membership_type']}.id =
                         {$this->_aliases['civicrm_membership']}.membership_type_id
           LEFT  JOIN civicrm_membership_type {$this->_aliases['civicrm_next_membership_type']}
                      ON {$this->_aliases['civicrm_next_membership_type']}.id =
                         {$this->_aliases['civicrm_membership']}.membership_type_id
                          ";

    if ($this->_employerField) {
      $this->_from .= "
           LEFT  JOIN  civicrm_relationship  {$this->_aliases['civicrm_relationship']} ON
                {$this->_aliases['civicrm_contact']}.id = {$this->_aliases['civicrm_relationship']}.contact_id_a AND {$this->_aliases['civicrm_relationship']}.relationship_type_id = 4
                AND {$this->_aliases['civicrm_relationship']}.is_active = 1
           LEFT  JOIN civicrm_contact {$this->_aliases['civicrm_contact_organization']} ON
                {$this->_aliases['civicrm_contact_organization']}.id = {$this->_aliases['civicrm_relationship']}.contact_id_b
                AND {$this->_aliases['civicrm_contact_organization']}.contact_type='Organization'\n";

    }
    //used when address field is selected
    if ($this->_addressField) {
      $this->_from .= "
         LEFT JOIN civicrm_address {$this->_aliases['civicrm_address']}
                   ON {$this->_aliases['civicrm_contact']}.id =
                      {$this->_aliases['civicrm_address']}.contact_id AND
                      {$this->_aliases['civicrm_address']}.is_primary = 1\n";
    }
    //used when email field is selected
    if ($this->_emailField) {
      $this->_from .= "
          LEFT JOIN civicrm_email {$this->_aliases['civicrm_email']}
                    ON {$this->_aliases['civicrm_contact']}.id =
                       {$this->_aliases['civicrm_email']}.contact_id AND
                       {$this->_aliases['civicrm_email']}.is_primary = 1\n";
    }
    //used when phone field is selected
    if ($this->_phoneField) {
      $this->_from .= "
          LEFT JOIN civicrm_phone {$this->_aliases['civicrm_phone']}
                    ON {$this->_aliases['civicrm_contact']}.id =
                       {$this->_aliases['civicrm_phone']}.contact_id AND
                       {$this->_aliases['civicrm_phone']}.is_primary = 1\n";
    }
  }

  function where() {
    $clauses = array();

    $diridsql = "SELECT id FROM civicrm_location_type WHERE name = 'Directory'";
    $dirid = 7;
    // Default directory location id in current install
    $dao = CRM_Core_DAO::executeQuery($diridsql);
    if ($dao->fetch()) {
      $dirid = $dao->id;
    }
    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('filters', $table)) {
        foreach ($table['filters'] as $fieldName => $field) {
          $clause = NULL;
          if ($fieldName == 'has_duplicate_mechs') {
            if ($this->_params['has_duplicate_mechs_value'] != '') {
              $clause = $this->getDupMechsClause($dirid);
            }
          }
          elseif (CRM_Utils_Array::value('operatorType', $field) & CRM_Utils_Type::T_DATE) {
            $relative = CRM_Utils_Array::value("{$fieldName}_relative", $this->_params);
            $from     = CRM_Utils_Array::value("{$fieldName}_from", $this->_params);
            $to       = CRM_Utils_Array::value("{$fieldName}_to", $this->_params);

            $clause = $this->dateClause($field['dbAlias'], $relative, $from, $to, $field['type']);
          }
          else {
            $op = CRM_Utils_Array::value("{$fieldName}_op", $this->_params);
            if ($op) {
              $clause = $this->whereClause($field,
                $op,
                CRM_Utils_Array::value("{$fieldName}_value", $this->_params),
                CRM_Utils_Array::value("{$fieldName}_min", $this->_params),
                CRM_Utils_Array::value("{$fieldName}_max", $this->_params));
            }
          }
          if (!empty($clause)) {
            $clauses[] = $clause;
          }
        }
      }
    }
    $clauses[] = "{$this->_aliases['civicrm_membership']}.is_test = 0";

    if (empty($clauses)) {
      $this->_where = "WHERE ( 1 ) ";
    }
    else {
      $this->_where = "WHERE " . implode(' AND ', $clauses);
    }
    if ($this->_aclWhere) {
      $this->_where .= " AND {$this->_aclWhere} ";
    }
  }

  function groupBy() {
    $this->_groupBy = " GROUP BY {$this->_aliases['civicrm_contact']}.id, {$this->_aliases['civicrm_membership']}.membership_type_id";
  }

  function orderBy() {
    $this->_orderBy = " ORDER BY {$this->_aliases['civicrm_contact']}.sort_name, {$this->_aliases['civicrm_contact']}.id, {$this->_aliases['civicrm_membership']}.membership_type_id";
  }

  function postProcess() {
    $this->beginPostProcess();
    $this->buildACLClause(array($this->_aliases['civicrm_contact']));
    if (isset($_REQUEST['task']) && $_REQUEST['task'] == "report_instance.csv"){
       $sql  = $this->buildQuery(FALSE);
       // CRM_Core_Error::debug('sql', $sql);
       // drupal_set_message($sql);
       // file_put_contents('/home/raic/public_html/debug.log',":::" . $_REQUEST['task']  . ":::\n", FILE_APPEND);
       $rows = array();
      $this->buildRows($sql, $rows);
    } else {
      $sql  = $this->buildQuery(TRUE);
      $rows = array();
       $this->buildRows($sql, $rows);
       $this->formatDisplay($rows);
       $this->doTemplateAssignment($rows);
    }

    $this->endPostProcess($rows);

// file_put_contents('/home/raic/public_html/debug.log',":::" . $_REQUEST['task']  . ":::\n", FILE_APPEND);

  }

  function alterDisplay(&$rows) {
    // custom code to alter rows
    $entryFound = FALSE;
    $checkList  = array();
    // Bulk calculate taxes
    $prices_with_taxes = array();
    $rules = array();
    $contacts = array();
    $membership_types = CRM_Member_PseudoConstant::membershipType();

    if (function_exists('_raic_members_calc_tax_all')) {
      // module enabled
      $prices_with_taxes = _raic_members_calc_tax_all();

      // Bulk calculate next memberships
      $rules = _raic_members_get_current_rules();

      foreach ($rows as $rowNum => $row) {
        //'initial_membership_type_id', 'join_date', 'manual_approval_type_id'
        $contacts[$row['civicrm_contact_id']] = array(
          'initial_membership_type_id' => isset($row['civicrm_membership_membership_type_id']) ?
          $row['civicrm_membership_membership_type_id'] : NULL,
          'join_date' => isset($row['civicrm_membership_join_date']) ?
          $row['civicrm_membership_join_date'] : NULL,
          'manual_approval_type_id' => isset($row['civicrm_value_membership_approval_12_custom_42']) ?
          $row['civicrm_value_membership_approval_12_custom_42'] : NULL,
        );
      }

      _raic_members_get_next_memberships_bulk_contacts($rules, $contacts);
    }

    foreach ($rows as $rowNum => $row) {
      if (array_key_exists('civicrm_membership_next_membership_type', $row)) {
        $contact = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_contact($row['civicrm_contact_id']);
        $current_memberships = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_current_membership_for_contact($contact);
        $initial_membership = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_initial_membership($current_memberships);
        $allowed_memberships = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_next_membership_for_contact($contact, $initial_membership);
        // print_r($initial_membership['membership_name']); die();
        if (!empty($initial_membership['membership_name'])) {
          $rows[$rowNum]['civicrm_membership_next_membership_type'] = $initial_membership['membership_name'];
        }
        if (!empty($allowed_memberships[0]) && CRM_Member_PseudoConstant::membershipType($allowed_memberships[0], FALSE)) {
          $rows[$rowNum]['civicrm_membership_next_membership_type'] = CRM_Member_PseudoConstant::membershipType($allowed_memberships[0], FALSE);
        }
        $entryFound = TRUE;
      }
      if (array_key_exists('civicrm_membership_years_passed', $row)) {
        $yearsPassed = CRM_Raicmembership_BAO_Raicmembership::getYearsPassed($contact, $initial_membership);
        // print_r($initial_membership); die();
        if ($yearsPassed) {
          $rows[$rowNum]['civicrm_membership_years_passed'] = $yearsPassed;
        }
        $entryFound = TRUE;
      }
      if (!empty($this->_noRepeats) && $this->_outputMode != 'csv') {
        // not repeat contact display names if it matches with the one
        // in previous row
        $repeatFound = FALSE;
        foreach ($row as $colName => $colVal) {
          if (CRM_Utils_Array::value($colName, $checkList) &&
               is_array($checkList[$colName]) &&
               in_array($colVal, $checkList[$colName])) {
            $rows[$rowNum][$colName] = "";
            $repeatFound = TRUE;
          }
          if (in_array($colName, $this->_noRepeats)) {
            $checkList[$colName][] = $colVal;
          }
        }
      }
      // Handle gender, prefixes, suffixes, deceased
      if (array_key_exists('civicrm_contact_gender_id', $row)) {
        if ($value = $row['civicrm_contact_gender_id']) {
          $rows[$rowNum]['civicrm_contact_gender_id'] = $this->_genders[$value];
        }
        $entryFound = TRUE;
      }
      if (array_key_exists('civicrm_contact_prefix_id', $row)) {
        if ($value = $row['civicrm_contact_prefix_id']) {
          $rows[$rowNum]['civicrm_contact_prefix_id'] = $this->_prefixes[$value];
        }
        $entryFound = TRUE;
      }
      if (array_key_exists('civicrm_contact_suffix_id', $row)) {
        if ($value = $row['civicrm_contact_suffix_id']) {
          $rows[$rowNum]['civicrm_contact_suffix_id'] = $this->_suffixes[$value];
        }
        $entryFound = TRUE;
      }
      if (array_key_exists('civicrm_contact_preferred_language', $row)) {
        if ($value = $row['civicrm_contact_preferred_language']) {
          $rows[$rowNum]['civicrm_contact_preferred_language'] = $this->_langs[$value];
        }
        $entryFound = TRUE;
      }
      if (array_key_exists('civicrm_contact_is_deceased', $row)) {
        if (!is_null($value = $row['civicrm_contact_is_deceased'])) {
          $rows[$rowNum]['civicrm_contact_is_deceased'] = $this->_booleans[$value];
        }
        $entryFound = TRUE;
      }
      // convert Organization display name to links
      if (array_key_exists('civicrm_contact_organization_organization_name', $row) &&
        CRM_Utils_Array::value('civicrm_contact_organization_organization_name', $rows[$rowNum]) && array_key_exists('civicrm_contact_organization_id', $row)) {
        $url = CRM_Utils_System::url("civicrm/contact/view", 'reset=1&cid=' . $rows[$rowNum]['civicrm_contact_organization_id'], $this->_absoluteUrl);
        $rows[$rowNum]['civicrm_contact_organization_organization_name_link'] = $url;
        $rows[$rowNum]['civicrm_contact_organization_organization_name_hover'] = ts("View Contact Summary for this Organization.");
      }
      // Handle membership
      if (array_key_exists('civicrm_membership_membership_type_id', $row)) {
        if ($value = $row['civicrm_membership_membership_type_id']) {
          $rows[$rowNum]['civicrm_membership_membership_type_id'] = CRM_Member_PseudoConstant::membershipType($value, FALSE);
        }
        $entryFound = TRUE;
      }
      // Used later for tax calculation, need to save the raw value
      $next_membership_id_saved = 0;
      // Handle getting the next membership type
      if (array_key_exists('civicrm_membership_next_membership_type', $row) &&
          array_key_exists('civicrm_contact_id', $row)) {
        if ($contact_id = $row['civicrm_contact_id']) {
          $next_memberships = $contacts[$row['civicrm_contact_id']]['allowed'];
          if (!empty($next_memberships)) {
            // Non-integer, text value means warning message, eg. rules failed, just print it
            if (is_numeric($next_memberships[0])) {
              $next_membership_id_saved = $next_memberships[0];
              $rows[$rowNum]['civicrm_membership_next_membership_type'] = isset($membership_types[$next_membership_id_saved]) ?
                $membership_types[$next_membership_id_saved] : $next_membership_id_saved;
            }
            else {
              $rows[$rowNum]['civicrm_membership_next_membership_type'] = $next_memberships[0];
            }
          }
        }
        $entryFound = TRUE;
      }
      // Used later for tax calculation, need to save the raw value
      $membership_id_saved = 0;
      if (array_key_exists('civicrm_membership_membership_type_id', $row) &&
        array_key_exists('civicrm_contact_id', $row)) {
        if ($contact_id = $row['civicrm_contact_id']) {
          // Non-integer, text value means warning message, eg. rules failed, just print it
          if (intval($row['civicrm_membership_membership_type_id'])) {
            $membership_id_saved = $row['civicrm_membership_membership_type_id'];
            $rows[$rowNum]['civicrm_membership_membership_type_id'] = isset($membership_types[$membership_id_saved]) ?
              $membership_types[$membership_id_saved] : $membership_id_saved;
          }
        }
        $entryFound = TRUE;
      }
      if (array_key_exists('civicrm_membership_years_passed', $row) &&
          array_key_exists('civicrm_contact_id', $row)) {
        if ($contact_id = $row['civicrm_contact_id']) {
          if (isset($contacts[$row['civicrm_contact_id']]['years_passed'])) {
            $rows[$rowNum]['civicrm_membership_years_passed'] = $contacts[$row['civicrm_contact_id']]['years_passed'];
          }
        }
      }
      // Handle tax calculation and total amount for original membership
      //_raic_tokens_gettaxinfo($state_province_id, $minimum_fee) from raic_tokens.module
      // Needs to come before state_province_id rewrite
      if (array_key_exists('civicrm_address_country_id', $row) &&
        array_key_exists('civicrm_address_state_province_id', $row) &&
        array_key_exists('civicrm_membership_membership_type_id', $row)) {
        if ($row['civicrm_address_country_id'] == CANADA_COUNTRY_CODE) {
          $state_province_id = $row['civicrm_address_state_province_id'];
          if (empty($state_province_id)) {
            $rows[$rowNum]['civicrm_membership_type_tax_payable'] = 'Missing Province';
            $rows[$rowNum]['civicrm_membership_type_total_payable'] = 'Missing Province';
          }
          else {
            if (isset($prices_with_taxes[$membership_id_saved][$state_province_id])) {
              $rows[$rowNum]['civicrm_membership_type_minimum_fee']
                = $prices_with_taxes[$membership_id_saved]['base_price'];
              $rows[$rowNum]['civicrm_membership_type_tax_payable']
                = $prices_with_taxes[$membership_id_saved][$state_province_id]['total_tax'];
              $rows[$rowNum]['civicrm_membership_type_total_payable']
                = $prices_with_taxes[$membership_id_saved]['base_price']
                + $prices_with_taxes[$membership_id_saved][$state_province_id]['total_tax'];
            }
            else {
              $rows[$rowNum]['civicrm_membership_type_total_payable'] = 'Unknown';
            }
          }
        }
        else {
          $rows[$rowNum]['civicrm_membership_type_minimum_fee'] = $prices_with_taxes[$next_membership_id_saved]['base_price'];
          $rows[$rowNum]['civicrm_membership_type_total_payable'] = $prices_with_taxes[$next_membership_id_saved]['base_price'];
        }
      }
      // Handle tax calculation and total amount for next type
      // Needs to come before state_province_id rewrite
      if (array_key_exists('civicrm_address_country_id', $row) &&
        array_key_exists('civicrm_address_state_province_id', $row) &&
        array_key_exists('civicrm_membership_next_membership_type', $row)) {
        if ($row['civicrm_address_country_id'] == CANADA_COUNTRY_CODE) {
          $state_province_id = $row['civicrm_address_state_province_id'];
          if (isset($prices_with_taxes[$next_membership_id_saved][$state_province_id])) {
            $rows[$rowNum]['civicrm_next_membership_type_next_minimum_fee']
              = $prices_with_taxes[$next_membership_id_saved]['base_price'];
            $rows[$rowNum]['civicrm_next_membership_type_next_tax_payable']
              = $prices_with_taxes[$next_membership_id_saved][$state_province_id]['total_tax'];
            $rows[$rowNum]['civicrm_next_membership_type_next_total_payable']
              = $prices_with_taxes[$next_membership_id_saved]['base_price']
              + $prices_with_taxes[$next_membership_id_saved][$state_province_id]['total_tax'];
          }
          else {
            $rows[$rowNum]['civicrm_next_membership_type_next_total_payable'] = 'Unknown';
          }
        }
        else {
          $rows[$rowNum]['civicrm_next_membership_type_next_minimum_fee'] = $prices_with_taxes[$next_membership_id_saved]['base_price'];
          $rows[$rowNum]['civicrm_next_membership_type_next_total_payable'] = $prices_with_taxes[$next_membership_id_saved]['base_price'];
        }
      }
      // Rewrite custom membership approval
      if (array_key_exists('civicrm_value_membership_approval_12_custom_42', $row)) {
        if (intval($row['civicrm_value_membership_approval_12_custom_42'])) {
          $rows[$rowNum]['civicrm_value_membership_approval_12_custom_42'] = CRM_Member_PseudoConstant::membershipType($row['civicrm_value_membership_approval_12_custom_42'], FALSE);
        }
      }
      // Handle Address rewrites
      if (array_key_exists('civicrm_address_state_province_id', $row)) {
        if ($value = $row['civicrm_address_state_province_id']) {
          $rows[$rowNum]['civicrm_address_state_province_id'] = CRM_Core_PseudoConstant::stateProvince($value, FALSE);
        }
        $entryFound = TRUE;
      }

      if (array_key_exists('civicrm_address_country_id', $row)) {
        if ($value = $row['civicrm_address_country_id']) {
          $rows[$rowNum]['civicrm_address_country_id'] = CRM_Core_PseudoConstant::country($value, FALSE);
        }
        $entryFound = TRUE;
      }

      if (array_key_exists('civicrm_contact_sort_name', $row) &&
          $rows[$rowNum]['civicrm_contact_sort_name'] &&
          array_key_exists('civicrm_contact_id', $row)) {
        $url = CRM_Utils_System::url("civicrm/contact/view", 'reset=1&cid=' . $row['civicrm_contact_id'], $this->_absoluteUrl);
        $rows[$rowNum]['civicrm_contact_sort_name_link'] = $url;
        $rows[$rowNum]['civicrm_contact_sort_name_hover'] = ts("View Contact Summary for this Contact.");
        $entryFound = TRUE;
      }

      if (!$entryFound) {
        break;
      }
    }
  }

  // Simplified from core OrganizationSummary.php
  function validRelationships() {
    $this->relationTypes = $relationTypes = array();

    $params = array('contact_type_a' => 'Individual', 'version' => 3);
    $typesA =& civicrm_api('relationship_type', 'get', $params);

    if (!CRM_Utils_Array::value('is_error', $typesA)) {
      foreach ($typesA['values'] as $rel) {
        $relationTypes[$rel['id']] = $rel['label_a_b'];
      }
    }

    $this->relationTypes = $relationTypes;
  }

  function getDupMechsClause($dirid) {
    $sql = "EXISTS
      (
        SELECT 1 FROM
        civicrm_contact AS civicrm_contact_dm_subq
        INNER JOIN civicrm_address AS civicrm_address_dm_subq
        ON civicrm_contact_dm_subq.id = civicrm_address_dm_subq.contact_id
          AND civicrm_address_dm_subq.location_type_id = {$dirid}
        WHERE civicrm_contact_dm_subq.id = {$this->_aliases['civicrm_contact']}.id
        GROUP BY civicrm_contact_dm_subq.id HAVING COUNT(*) > 1
        UNION
        SELECT 1 FROM
        civicrm_contact AS civicrm_contact_dm_subq2
        INNER JOIN civicrm_phone AS civicrm_phone_dm_subq
        ON civicrm_contact_dm_subq2.id = civicrm_phone_dm_subq.contact_id
          AND civicrm_phone_dm_subq.location_type_id = {$dirid} AND civicrm_phone_dm_subq.phone_type_id = 1
        WHERE civicrm_contact_dm_subq2.id = {$this->_aliases['civicrm_contact']}.id
        GROUP BY civicrm_contact_dm_subq2.id HAVING COUNT(*) > 1
        UNION
        SELECT 1 FROM
        civicrm_contact AS civicrm_contact_dm_subq3
        INNER JOIN civicrm_phone AS civicrm_phone_dm_subq2
        ON civicrm_contact_dm_subq3.id = civicrm_phone_dm_subq2.contact_id
          AND civicrm_phone_dm_subq2.location_type_id = {$dirid} AND civicrm_phone_dm_subq2.phone_type_id = 3
        WHERE civicrm_contact_dm_subq3.id = {$this->_aliases['civicrm_contact']}.id
        GROUP BY civicrm_contact_dm_subq3.id HAVING COUNT(*) > 1
        UNION
        SELECT 1 FROM
        civicrm_contact AS civicrm_contact_dm_subq4
        INNER JOIN civicrm_email AS civicrm_email_dm_subq
        ON civicrm_contact_dm_subq4.id = civicrm_email_dm_subq.contact_id
          AND civicrm_email_dm_subq.location_type_id = {$dirid}
        WHERE civicrm_contact_dm_subq4.id = {$this->_aliases['civicrm_contact']}.id
        GROUP BY civicrm_contact_dm_subq4.id HAVING COUNT(*) > 1
        UNION
        SELECT 1 FROM
        civicrm_contact AS civicrm_contact_dm_subq5
        INNER JOIN civicrm_website AS civicrm_website_dm_subq
        ON civicrm_contact_dm_subq5.id = civicrm_website_dm_subq.contact_id
          AND civicrm_website_dm_subq.website_type_id = 6
        WHERE civicrm_contact_dm_subq5.id = {$this->_aliases['civicrm_contact']}.id
        GROUP BY civicrm_contact_dm_subq5.id HAVING COUNT(*) > 1
      )";
    return $sql;
  }

  function alterCustomDataDisplay(&$rows) {
    // custom code to alter rows having custom values
    if (empty($this->_customGroupExtends)) {
      return;
    }

    $customFieldIds = array();
    foreach ($this->_params['fields'] as $fieldAlias => $value) {
      if ($fieldId = CRM_Core_BAO_CustomField::getKeyID($fieldAlias)) {
        $customFieldIds[$fieldAlias] = $fieldId;
      }
    }
    if (empty($customFieldIds)) {
      return;
    }

    $customFields = $fieldValueMap = array();
    $customFieldCols = array('column_name', 'data_type', 'html_type', 'option_group_id', 'id');

    // skip for type date and ContactReference since date format is already handled
    $query = "
SELECT cg.table_name, cf." . implode(", cf.", $customFieldCols) . ", ov.value, ov.label
FROM  civicrm_custom_field cf
INNER JOIN civicrm_custom_group cg ON cg.id = cf.custom_group_id
LEFT JOIN civicrm_option_value ov ON cf.option_group_id = ov.option_group_id
WHERE cg.extends IN ('" . implode("','", $this->_customGroupExtends) . "') AND
      cg.is_active = 1 AND
      cf.is_active = 1 AND
      cf.is_searchable = 1 AND
      cf.data_type   NOT IN ('ContactReference', 'Date') AND
      cf.id IN (" . implode(",", $customFieldIds) . ")";

    $dao = CRM_Core_DAO::executeQuery($query);
    while ($dao->fetch()) {
      foreach ($customFieldCols as $key) {
        $customFields[$dao->table_name . '_custom_' . $dao->id][$key] = $dao->$key;
      }
      if ($dao->option_group_id) {
        $fieldValueMap[$dao->option_group_id][$dao->value] = $dao->label;
      }
    }
    $dao->free();

    $entryFound = FALSE;
    foreach ($rows as $rowNum => $row) {
      foreach ($row as $tableCol => $val) {
        if (array_key_exists($tableCol, $customFields) && array_search($tableCol, $this->_customDoNotAlter) === FALSE) {
          // $rows[$rowNum][$tableCol] = $this->formatCustomValues($val, $customFields[$tableCol], $fieldValueMap);
          $rows[$rowNum][$tableCol] = CRM_Core_BAO_CustomField::displayValue($val, $customFields[$tableCol]);
          $entryFound = TRUE;
        }
      }

      // skip looking further in rows, if first row itself doesn't
      // have the column we need
      if (!$entryFound) {
        break;
      }
    }
  }

}
