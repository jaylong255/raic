This Extension:
--------------
Built for the Moriyama Gala: https://www.raic.org/civicrm/event/register?reset=1&id=81

Sets the price field to be 0 instead of blank
