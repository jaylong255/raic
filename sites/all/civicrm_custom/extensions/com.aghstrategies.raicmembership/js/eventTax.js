CRM.$(function ($) {
  var getOptionsBasedOnProvince = function () {
    var raicprefix = CRM.vars.raicmembership.raicprefix;
    var provinceId = 'default';
    if ($('#billing_state_province_id-5').val() !== '') {
      if (typeof ($('#billing_state_province_id-5').val()) !== 'undefined') {
        provinceId = $('#billing_state_province_id-5').val();
      }
    }
    if (CRM.vars.raicmembership.finTypes) {
      var finTypeToUse = parseInt(CRM.vars.raicmembership.finTypes[raicprefix + provinceId]);

      var processedFields = [];
      $.each($('input[type=radio][name^="price_"]'), function (index, radioOption) {
        // Price fields will have multiple radio options, so we don't want to
        // repeat them.  Returning non-false in a callback of $.each is the same
        // as `continue`.
        var fieldName = $(radioOption).attr('name');
        if (processedFields.includes(fieldName)) {
          return true;
        }

        // Likewise skip this if there are no price options
        var optionsJson = $(radioOption).attr('data-price-field-values');
        if (typeof (optionsJson) === 'undefined') {
          return true;
        }

        // Each price option contains data for all of them in the price field.
        var priceOptions = JSON.parse(optionsJson);

        // We'll want to stash the ID of the corresponding option in case the
        // selected one becomes hidden because someone has edited their address.
        var rightWebOption = 0;
        var clickRightWebOption = false;

        for (var optVal in priceOptions) {
          // The in-person option should always be there.  It also should never
          // be the option selected when you switch your address and you had the
          // wrong web option selected.
          if (CRM.vars.raicmembership.inPersonPriceOptions.includes(parseInt(optVal))) {
            continue;
          }

          var $thatOption = $('input[name="' + fieldName + '"][value=' + optVal + ']');
          if (priceOptions[optVal].financial_type_id == finTypeToUse) {
            rightWebOption = optVal;
            $thatOption.parent().show();
          } else {
            if ($thatOption.prop('checked')) {
              clickRightWebOption = true;
            }

            $thatOption.parent().hide();
          }
        }

        if (clickRightWebOption) {
          // TODO: Somehow this needs to be clicked twice to calculate properly.
          // (Clicking too many times seems not to cause problems.)
          $('input[name="' + fieldName + '"][value=' + rightWebOption + ']').click().click();
        }
      });
    }
  };

  getOptionsBasedOnProvince();
  $('#billing_state_province_id-5').change(function () {
    getOptionsBasedOnProvince();
  });
});
