CRM.$(function ($) {
  // AGH #11287 Freeze name on membership renewal form
  $('#first_name').prop('disabled', true);
  $('#last_name').prop('disabled', true);

  // array matching financial types and provinces
  var finTypes = CRM.vars.raicmembership.finTypes;

  // array of allowed membership types sent from php
  var allowedMemberships = CRM.vars.raicmembership.memTypes;
  $.each(allowedMemberships, function (index, mem) {
    allowedMemberships[index] = parseInt(mem);
  });

  // get price fields on the page
  var priceFields = [];
  $.each($('input[type=radio][name^="price_"]'), function (index, value) {
    var field = parseInt($(value).attr('name').slice((0, 6)));
    if ($.inArray(field, priceFields) == -1) {
      priceFields.push(field);
    }
  });

  var getFinTypeToUse = function () {
    // get province id of one exists otherwise use default

    var provinceId = 'default';
    if ($('#billing_country_id-5').length &&
      $('#billing_country_id-5').val() == 1039 &&
      $('#billing_state_province_id-5').length &&
      $('#billing_state_province_id-5').val().length
    ) {
      provinceId = $('#billing_state_province_id-5').val();
    }

    // get province that matches financial type
    var finTypeToUse = parseInt(finTypes['membership_' + provinceId]);
    return finTypeToUse;
  };

  var getOptionsBasedOnProvince = function () {
    var finTypeToUse = getFinTypeToUse();
    var optionFound = false;

    // go thru priceFields and find options that have the right financial type and membership type
    $(priceFields).each(function (index, field) {
      var vo = [];
      var priceOptions = $.parseJSON(
        $('input[type=radio][name="price_' + field + '"]').attr('data-price-field-values')
      );

      $('input[name="price_' + field + '"][value=0]').parent().show();
      $.each(priceOptions, function (key, object) {
        $('[name="price_' + field + '"][value=' + key + ']').parent().show();
        var membershipType = parseInt(object.membership_type_id);
        if (
        parseInt(object.financial_type_id) == finTypeToUse
        && allowedMemberships.indexOf(parseInt(object.membership_type_id)) > -1) {
          vo.push(key);
        } else {
          $('[name="price_' + field + '"][value=' + key + ']').parent().hide();
        }

      });

      $('input[name="price_' + field + '"][value=0]').parent().hide();
      if (!vo.length) {
        $('input[name="price_' + field + '"][value=0]').prop('checked', true).click();
        $('input[name="price_' + field + '"][value=0]').parent().parent().parent().parent().hide();
      }

      if (vo.length > 0) {
        $('input[name="price_' + field + '"][value=' + vo[0] + ']').prop('checked', true).click();
        $('input[name="price_' + field + '"][value=' + vo[0] + ']')
        .parent().parent().parent().parent().show();
        var optionFound = true;
        $('input[name="price_' + field + '"][value=0]').parent().hide();
      }
    });

    if (optionFound == false) {
      var $div = $('<div>', {
        id: 'foo',
        class: 'a',
        content: 'We could not find a membership renewal option for you please contact: XXX',
      });
      $($div).insertBefore('#membership');
    }

    $('div#membership').css('background-image', 'none');
    $('div#priceset').css('opacity', '1');
  };

  getOptionsBasedOnProvince();
  $('#billing_state_province_id-5').change(function () {
    getOptionsBasedOnProvince();
  });

  $('input[type=radio][name^="price_"]').change(function () {
    getOptionsBasedOnProvince();
  });
});
