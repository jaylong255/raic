CRM.$(function ($) {
  var allowedMemberships = CRM.vars.raicmembership.memTypes;
  var financialType = CRM.vars.raicmembership.fin_types;
  adjustMembershipOrgType();

  // Set financial type id on renewal page
  var setFinType = function () {
    if (financialType.financial_type_id) {
      console.log(financialType.financial_type_id);
      $('select#financial_type_id').val(financialType.financial_type_id);
    }
  };

  // set default membership type for renewals
  if (allowedMemberships[0]) {
    adjustMembershipOrgType();
    $('#membership_type_id_1').val(allowedMemberships[0]).trigger('change');
  }

  setFinType();
  $('#membership_type_id_1').change(setFinType);
});
