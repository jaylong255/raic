CRM.$(function ($) {
  var financialType = CRM.vars.raicmembership.fin_types;
  var priceSet = CRM.vars.raicmembership.price_set;

  // On add membership page when a membership type is selected update the financial type
  $('#membership_type_id_1').change(function () {
    if (financialType.financial_type_id) {
      $('select#financial_type_id').val(financialType.financial_type_id);
    }
  });
  if (priceSet > 0) {
    $('#price_set_id').val(priceSet).change();
  }
});
