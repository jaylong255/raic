<?php

/**
 * Form controller class
 *
 * @see https://wiki.civicrm.org/confluence/display/CRMDOC/QuickForm+Reference
 */
class CRM_Raicmembership_Form_TaxesSettings extends CRM_Core_Form {
  public function provinceOptions() {
    //TODO could create this array by doing an api call
    return array(
      1100 => "Alberta",
      1101 => "British Columbia",
      1102 => "Manitoba",
      1103 => "New Brunswick",
      1104 => "Newfoundland and Labrador",
      1105 => "Northwest Territories",
      1106 => "Nova Scotia",
      1107 => "Nunavut",
      1108 => "Ontario",
      1109 => "Prince Edward Island",
      1110 => "Quebec",
      1111 => "Saskatechewan",
      1112 => "Yukon Territory",
    );
  }

  public function buildQuickForm() {
    // TODO make for each province
    $provinces = self::provinceOptions();
    foreach ($provinces as $id => $name) {
      $this->addEntityRef("membership_{$id}", ts("Membership: $name ($id)"), array(
        'entity' => 'financialType',
        'placeholder' => ts('- Select Financial Type -'),
        'select' => array('minimumInputLength' => 0),
      ));
      $this->addEntityRef("event_{$id}", ts("Event: $name ($id)"), array(
        'entity' => 'financialType',
        'placeholder' => ts('- Select Financial Type -'),
        'select' => array('minimumInputLength' => 0),
      ));
      $this->addEntityRef("payment_{$id}", ts("Payment: $name ($id)"), array(
        'entity' => 'financialType',
        'placeholder' => ts('- Select Financial Type -'),
        'select' => array('minimumInputLength' => 0),
      ));
    }
    $this->addEntityRef('membership_default', ts("Membership: Default if no Province"), array(
      'entity' => 'financialType',
      'placeholder' => ts('- Select Financial Type -'),
      'select' => array('minimumInputLength' => 0),
    ));
    $this->addEntityRef('event_default', ts("Event: Default if no Province"), array(
      'entity' => 'financialType',
      'placeholder' => ts('- Select Financial Type -'),
      'select' => array('minimumInputLength' => 0),
    ));
    $this->addEntityRef('payment_default', ts("Payment: Default if no Province"), array(
      'entity' => 'financialType',
      'placeholder' => ts('- Select Financial Type -'),
      'select' => array('minimumInputLength' => 0),
    ));

    // contribution page to apply taxes to
    $this->addEntityRef('contribpagesps', ts('Contribution Pages Price Sets'), array(
      'entity' => 'PriceSet',
      'api' => array(
        'params' => array('extends' => 'CiviContribute'),
      ),
      'placeholder' => ts('- Event Pages Price Sets -'),
      'select' => array('minimumInputLength' => 0),
      'multiple' => TRUE,
    ));

    // Event page to apply taxes to
    $this->addEntityRef('eventpagesps', ts('Event Pages Price Sets'), array(
      'entity' => 'PriceSet',
      'api' => array(
        'params' => array('extends' => 'CiviEvent'),
      ),
      'placeholder' => ts('- Event Pages Price Sets -'),
      'select' => array('minimumInputLength' => 0),
      'multiple' => TRUE,
    ));

    $this->addButtons(array(
      array(
        'type' => 'submit',
        'name' => ts('Submit'),
        'isDefault' => TRUE,
      ),
    ));
    $defaults = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_financial_types();
    $this->setDefaults($defaults);
    // export form elements
    $this->assign('elementNames', $this->getRenderableElementNames());
    parent::buildQuickForm();
  }

  public function postProcess() {
    $values = $this->exportValues();
    $provinces = self::provinceOptions();
    $taxSettings = array();
    foreach ($provinces as $id => $name) {
      if (!empty($values["event_{$id}"])) {
        $taxSettings["event_{$id}"] = $values["event_{$id}"];
      }
      if (!empty($values["membership_{$id}"])) {
        $taxSettings["membership_{$id}"] = $values["membership_{$id}"];
      }
      if (!empty($values["payment_{$id}"])) {
        $taxSettings["payment_{$id}"] = $values["payment_{$id}"];
      }
    }

    $otherFields = array(
      'event_default',
      'membership_default',
      'payment_default',
      'contribpagesps',
      'eventpagesps',
    );
    foreach ($otherFields as $key => $field) {
      if (!empty($values[$field])) {
        $taxSettings[$field] = $values[$field];
      }
    }
    if (!empty($taxSettings)) {
      try {
        $result = civicrm_api3('Setting', 'create', array('raicmembership_provincetaxes' => $taxSettings));
      }
      catch (CiviCRM_API3_Exception $e) {
        $error = $e->getMessage();
        CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicmembership')));
        CRM_Core_Session::setStatus(ts('Error saving pages for priceset buttons', array('domain' => 'com.aghstrategies.raicmembership')), 'Error', 'error');
      }
      if ($result['is_error'] == 0) {
        CRM_Core_Session::setStatus(ts('You have successfully updated the province taxes settings.', array('domain' => 'com.aghstrategies.raicmembership')), 'Settings saved', 'success');
      }
    }

    parent::postProcess();
  }

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  public function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
    // items don't have labels.  We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }

}
