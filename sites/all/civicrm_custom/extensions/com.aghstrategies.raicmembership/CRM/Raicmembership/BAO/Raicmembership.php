<?php
/**
 *
 * @package BOT Roundlake Payment User Interface Helper functions
 * $Id$
 *
 */
class CRM_Raicmembership_BAO_Raicmembership {

  public static function getYearsPassed($contact, $initial_membership = NULL) {
    $years_passed = 'not found';
    $allowed = array();
    // If there is no Manual Approval use the Rules array
    if (empty($contact['custom_' . MEMBERSHIP_APPROVAL_FIELD_ID])) {
      if ($initial_membership == NULL) {
        // We don't know which one to renew
        return $allowed;
      }

      $join_date  = date_create_from_format('Y-m-d', $initial_membership['join_date']);
      $today = date_create();
      $elapsed = date_diff($join_date, $today);
      $years_passed = (int) (round(((float)$elapsed->m) / 12) + $elapsed->y);
    }
    return $years_passed;
  }

  public static function _raicmembership_get_current_rules() {
    $rules = array(
      MEMBERSHIP_TYPE_GRADUATE_CANADIAN => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_GRADUATE_CANADIAN)),
      ),
      MEMBERSHIP_TYPE_GRADUATE_CANADIAN_1ST_YEAR => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_GRADUATE_CANADIAN_1ST_YEAR)),
      ),
      MEMBERSHIP_TYPE_GRADUATE_CANADIAN_2ND_4TH_YEARS => array(
        array('years_passed' => 1, 'next_type' => array(MEMBERSHIP_TYPE_GRADUATE_CANADIAN_2ND_4TH_YEARS)),
        array('years_passed' => 2, 'next_type' => array(MEMBERSHIP_TYPE_GRADUATE_CANADIAN_2ND_4TH_YEARS)),
        array('years_passed' => 3, 'next_type' => array(MEMBERSHIP_TYPE_GRADUATE_CANADIAN)),
      ),
      MEMBERSHIP_TYPE_INTERN_ARCHITECT_1ST_5_YEARS => array(
        array('years_passed' => 1, 'next_type' => array(MEMBERSHIP_TYPE_INTERN_ARCHITECT_1ST_5_YEARS)),
        array('years_passed' => 2, 'next_type' => array(MEMBERSHIP_TYPE_INTERN_ARCHITECT_1ST_5_YEARS)),
        array('years_passed' => 3, 'next_type' => array(MEMBERSHIP_TYPE_INTERN_ARCHITECT_1ST_5_YEARS)),
        array('years_passed' => 4, 'next_type' => array(MEMBERSHIP_TYPE_INTERN_ARCHITECT_1ST_5_YEARS)),
        array('years_passed' => 5, 'next_type' => array(MEMBERSHIP_TYPE_INTERN_ARCHITECT)),
      ),
      MEMBERSHIP_TYPE_INTERN_ARCHITECT => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_INTERN_ARCHITECT)),
      ),
      MEMBERSHIP_TYPE_GRADUATE_FOREIGN_SCHOOL_1ST_YEAR => array(
        array('years_passed' => 1, 'next_type' => array(MEMBERSHIP_TYPE_GRADUATE_FOREIGN_SCHOOL)),
      ),
      MEMBERSHIP_TYPE_GRADUATE_FOREIGN_SCHOOL => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_GRADUATE_FOREIGN_SCHOOL)),
      ),
      MEMBERSHIP_TYPE_INTERNATIONAL_ASSOCIATE_1ST_YEAR => array(
        array('years_passed' => 1, 'next_type' => array(MEMBERSHIP_TYPE_INTERNATIONAL_ASSOCIATE)),
      ),
      MEMBERSHIP_TYPE_INTERNATIONAL_ASSOCIATE => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_INTERNATIONAL_ASSOCIATE)),
      ),
      MEMBERSHIP_TYPE_REGISTERED_ARCHITECT_1ST_YEAR => array(
        array('years_passed' => 1, 'next_type' => array(MEMBERSHIP_TYPE_REGISTERED_ARCHITECT)),
      ),
      MEMBERSHIP_TYPE_REGISTERED_ARCHITECT_PROMO => array(
        array('years_passed' => 1, 'next_type' => array(MEMBERSHIP_TYPE_REGISTERED_ARCHITECT)),
      ),
      MEMBERSHIP_TYPE_REGISTERED_ARCHITECT => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_REGISTERED_ARCHITECT)),
      ),
      MEMBERSHIP_TYPE_AFFILIATE_INSTITUTE => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_AFFILIATE_INSTITUTE)),
      ),
      MEMBERSHIP_TYPE_AFFILIATE_INDIVIDUAL => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_AFFILIATE_INDIVIDUAL)),
      ),
      MEMBERSHIP_TYPE_AFFILIATE_FIRM_0_500 => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_AFFILIATE_FIRM_0_500)),
      ),
      MEMBERSHIP_TYPE_AFFILIATE_FIRM_500_2500 => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_AFFILIATE_FIRM_500_2500)),
      ),
      MEMBERSHIP_TYPE_AFFILIATE_FIRM_2500_10000 => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_AFFILIATE_FIRM_2500_10000)),
      ),
      MEMBERSHIP_TYPE_AFFILIATE_FIRM_10000_250000 => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_AFFILIATE_FIRM_10000_250000)),
      ),
      MEMBERSHIP_TYPE_AFFILIATE_FIRM_250000_PLUS => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_AFFILIATE_FIRM_250000_PLUS)),
      ),
      MEMBERSHIP_TYPE_FACULTY_1ST_YEAR => array(
        array('years_passed' => 1, 'next_type' => array(MEMBERSHIP_TYPE_FACULTY)),
      ),
      MEMBERSHIP_TYPE_FACULTY => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_FACULTY)),
      ),
      MEMBERSHIP_TYPE_RETIRED => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_RETIRED)),
      ),
      MEMBERSHIP_TYPE_FELLOW_REGULAR => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_FELLOW_REGULAR)),
      ),
      MEMBERSHIP_TYPE_FELLOW_RETIRED => array(
        array('years_passed' => -1, 'next_type' => array(MEMBERSHIP_TYPE_FELLOW_RETIRED)),
      ),
    );
    return $rules;
  }

  public static function _raicmembership_get_current_membership_for_contact($contact = NULL) {
    $current_memberships = array();

    if ($contact == NULL) {
      return $current_memberships;
    }

    $params = array(
      'version' => 3,
      'contact_id' => $contact['id'],
      'is_test' => 0,
    );
    $memberships = civicrm_api('Membership', 'get', $params);
    if ($memberships['is_error'] == 1) {
      return $current_memberships;
    }

    foreach ($memberships['values'] as $id => $membership) {
      $current_memberships[$membership['status_id']][] = $membership;
    }

    return $current_memberships;
  }

  public static function _raicmembership_get_initial_membership($current_memberships) {
    $initial_membership = NULL;
    // New, Current, Grace, Expired
    $status_search_order = array(1, 2, 3, 4);
    foreach ($status_search_order as $status) {
      if (isset($current_memberships[$status]) && count($current_memberships[$status]) == 1) {
        $initial_membership = reset($current_memberships[$status]);
        break;
      }
    }

    return $initial_membership;
  }

  public static function _raicmembership_get_next_membership_for_contact($contact, $initial_membership = NULL) {

    $allowed = array();
    // If there is no Manual Approval use the Rules array
    if (empty($contact['custom_' . MEMBERSHIP_APPROVAL_FIELD_ID])) {
      if ($initial_membership == NULL) {
        // We don't know which one to renew
        return $allowed;
      }

      $join_date  = date_create_from_format('Y-m-d', $initial_membership['join_date']);
      $today = date_create();
      $elapsed = date_diff($join_date, $today);
      $years_passed = (int) (round(((float)$elapsed->m) / 12) + $elapsed->y);
      // TODO: Figure out how to do years passed initially with old data and invalid membership logs
      if (isset($initial_membership)) {
        $rules = self::_raicmembership_get_current_rules();
        if (is_array($rules[$initial_membership['membership_type_id']])) {
          $max_allowed_by_rule = 0;
          $last_type_seen = 0;
          foreach ($rules[$initial_membership['membership_type_id']] as $rule) {
            if ($max_allowed_by_rule < $rule['years_passed']) {
              $max_allowed_by_rule = $rule['years_passed'];
              $last_type_seen = $rule['next_type'];
            }
            if ($rule['years_passed'] == -1 || $rule['years_passed'] == $years_passed) {
              $allowed += $rule['next_type'];
            }
          }
          // If we have looked at all the rules and not found a match and $years_passed is still
          // greater than we have rules for then take the last next type for that rule
          if (empty($allowed) && $max_allowed_by_rule > 0 && $years_passed > $max_allowed_by_rule) {
            $allowed += $last_type_seen;
          }
        }
      }
    }
    // Use the Manual approval
    else {
      $allowed[] = $contact['custom_' . MEMBERSHIP_APPROVAL_FIELD_ID];
    }

    return $allowed;
  }

  public static function _raicmembership_get_current_contact($form = NULL) {
    if (is_a($form, 'CRM_Contribute_Form_Contribution_Main') ||
      is_a($form, 'CRM_Contribute_Form_Contribution_ThankYou')) {
      $userId = $form->getContactID();
    }
    else {
      $session = CRM_Core_Session::singleton();
      $userId = $session->get('userID');
    }

    $contact = self::_raicmembership_get_contact($userId);
    return $contact;
  }

  public static function _raicmembership_get_contact($userId = NULL) {
    $contact = NULL;
    if (!isset($userId)) {
      return $contact;
    }
    $standard_fields = array(
      'contact_id',
      'contact_type',
      'contact_sub_type',
      'sort_name',
      'display_name',
      'email_id',
      'email',
      'id',
      'custom_' . MEMBERSHIP_APPROVAL_FIELD_ID,
    );

    $params = array(
      'version' => 3,
      'id' => $userId,
      'return' => $standard_fields,
      'sequential' => '0',
    );
    $contacts = civicrm_api('Contact', 'get', $params);
    if ($contacts['is_error'] == 1) {
      return NULL;
    }
    $contact_array = $contacts['values'][$userId];
    $address = array();
    // look up billing address
    try {
      $addresses = civicrm_api3('Address', 'get', array(
        'contact_id' => $userId,
        'is_billing' => 1,
      ));
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicmembership')));
    }
    // If no billing address search for primary
    if ($addresses['count'] >= 0) {
      try {
        $addresses = civicrm_api3('Address', 'get', array(
          'contact_id' => $userId,
          'is_primary' => 1,
        ));
      }
      catch (CiviCRM_API3_Exception $e) {
        $error = $e->getMessage();
        CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicmembership')));
      }
    }
    if ($addresses['is_error'] != 1) {
      $address = reset($addresses['values']);
      // Key clash
      $address['address_id'] = $address['id'];
      unset($address['id']);
    }
    $contact = array_merge($contact_array, $address);

    return $contact;
  }

  public static function _raicmembership_financial_types() {
    try {
      $result = civicrm_api3('Setting', 'get', array(
        'sequential' => 1,
        'return' => array("raicmembership_provincetaxes"),
      ));
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicmembership')));
    }
    if (!empty($result['values'][0]['raicmembership_provincetaxes'])) {
      return $result['values'][0]['raicmembership_provincetaxes'];
    }
  }

  public static function _raicmembership_priceSetForProvince($finType) {
    try {
      $result = civicrm_api3('PriceSet', 'getsingle', array(
        'sequential' => 1,
        'title' => array('LIKE' => "%Membership Taxes for%"),
        'financial_type_id' => $finType,
      ));
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicmembership')));
    }

    if (!empty($result)) {
      return $result['id'];
    }
    else {
      return 0;
    }
  }

}
