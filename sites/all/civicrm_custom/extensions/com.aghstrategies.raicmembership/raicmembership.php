<?php

require_once 'raicmembership.civix.php';

// Custom Approval Field ID
define('MEMBERSHIP_APPROVAL_FIELD_ID', 42);

// Membership Types
define('MEMBERSHIP_TYPE_STUDENT_ASSOCIATE', 1);
define('MEMBERSHIP_TYPE_GRADUATE_CANADIAN_1ST_YEAR', 26);
define('MEMBERSHIP_TYPE_GRADUATE_CANADIAN_2ND_4TH_YEARS', 27);
define('MEMBERSHIP_TYPE_GRADUATE_CANADIAN', 15);
define('MEMBERSHIP_TYPE_GRADUATE_FOREIGN_SCHOOL_1ST_YEAR', 22);
define('MEMBERSHIP_TYPE_GRADUATE_FOREIGN_SCHOOL', 19);
define('MEMBERSHIP_TYPE_INTERN_ARCHITECT_1ST_5_YEARS', 30);
define('MEMBERSHIP_TYPE_INTERN_ARCHITECT_4TH_YEAR', 31);
define('MEMBERSHIP_TYPE_INTERN_ARCHITECT', 17);
define('MEMBERSHIP_TYPE_INTERNATIONAL_ASSOCIATE_1ST_YEAR', 25);
define('MEMBERSHIP_TYPE_INTERNATIONAL_ASSOCIATE', 9);
define('MEMBERSHIP_TYPE_REGISTERED_ARCHITECT_1ST_YEAR', 29);
define('MEMBERSHIP_TYPE_REGISTERED_ARCHITECT', 20);
define('MEMBERSHIP_TYPE_REGISTERED_ARCHITECT_PROMO', 31);
define('MEMBERSHIP_TYPE_FACULTY_1ST_YEAR', 28);
define('MEMBERSHIP_TYPE_FACULTY', 14);
define('MEMBERSHIP_TYPE_FELLOW_REGULAR', 12);
define('MEMBERSHIP_TYPE_FELLOW_RETIRED', 24);
define('MEMBERSHIP_TYPE_RETIRED', 23);
define('MEMBERSHIP_TYPE_AFFILIATE_INDIVIDUAL', 2);
define('MEMBERSHIP_TYPE_AFFILIATE_INSTITUTE', 3);
define('MEMBERSHIP_TYPE_AFFILIATE_FIRM_0_500', 4);
define('MEMBERSHIP_TYPE_AFFILIATE_FIRM_500_2500', 5);
define('MEMBERSHIP_TYPE_AFFILIATE_FIRM_2500_10000', 6);
define('MEMBERSHIP_TYPE_AFFILIATE_FIRM_10000_250000', 7);
define('MEMBERSHIP_TYPE_AFFILIATE_FIRM_250000_PLUS', 8);

/**
 * Implements hook_civicrm_alterMailParams().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterMailParams
 */
function raicmembership_civicrm_alterMailParams(&$params, $context) {
  if ($context == 'messageTemplate') {
    // AGH #9805 offline renewals "renew credit card" and offline renewals by cheque - add CC receipt to asauve@raic.org
    if ($params['valueName'] == 'membership_offline_receipt') {
      $params['cc'] = "asauve@raic.org";
    }
  }
}

function raicmembership_civicrm_preProcess($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Contribution_Main' && $form->getVar('_id') == 57) {
    CRM_Core_Resources::singleton()->addStyleFile('com.aghstrategies.raicmembership', 'css/loading.css', 1, 'html-header');
    CRM_Core_Resources::singleton()->addScriptFile('com.aghstrategies.raicmembership', 'js/provinceChange.js', 1, 'html-header');
  }
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_buildForm
 */
function raicmembership_civicrm_buildForm($formName, &$form) {
  //NOTE if we need to limit renewal options on the backend the form name would be 'CRM_Member_Form_MembershipRenewal' and 'CRM_Member_Form_Membership'
  if ($formName == 'CRM_Contribute_Form_Contribution_Main' && $form->getVar('_id') == 57) {
    $contact = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_current_contact($form);
  }
  if ($formName == 'CRM_Member_Form_MembershipRenewal' || $formName == 'CRM_Member_Form_Membership') {
    $form->_defaultValues['record_contribution'] = 1;
    $contact = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_contact($form->_contactID);
  }
  if ($formName == 'CRM_Member_Form_MembershipRenewal' || $formName == 'CRM_Contribute_Form_Contribution_Main' && $form->getVar('_id') == 57 || $formName == 'CRM_Member_Form_Membership') {
    $current_memberships = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_current_membership_for_contact($contact);
    $initial_membership = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_initial_membership($current_memberships);
    $allowed_memberships = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_next_membership_for_contact($contact, $initial_membership);
    CRM_Core_Resources::singleton()->addVars('raicmembership', array('memTypes' => $allowed_memberships));
    $finTypes = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_financial_types();
    if ($formName == 'CRM_Member_Form_MembershipRenewal' || $formName == 'CRM_Member_Form_Membership') {
      if ($formName == 'CRM_Member_Form_MembershipRenewal') {
        CRM_Core_Resources::singleton()->addScriptFile('com.aghstrategies.raicmembership', 'js/backendMembershipRenewal.js');
      }
      if ($formName == 'CRM_Member_Form_Membership') {
        CRM_Core_Resources::singleton()->addScriptFile('com.aghstrategies.raicmembership', 'js/backendNewMembership.js');
      }
      // sets default financial type based on state province
      if ($contact['country_id'] == 1039 && !empty($finTypes["membership_{$contact['state_province_id']}"])) {
        $defaults = array('financial_type_id' => $finTypes["membership_{$contact['state_province_id']}"]);
      }
      else {
        CRM_Core_Session::setStatus(ts('No Province Found for this contact so correct membership taxes will not be provided automatically. Please be sure to pick the right financial type for the contact or go back and update their province on their contact record.'), 'Proceed With Caution', 'alert');
        $defaults = array('financial_type_id' => $finTypes["membership_{$finTypes['default']}"]);
      }
      $priceSetForNew = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_priceSetForProvince($defaults['financial_type_id']);
      if ($priceSetForNew > 0) {
        CRM_Core_Resources::singleton()->addVars('raicmembership', array('price_set' => $priceSetForNew));
      }
      CRM_Core_Resources::singleton()->addVars('raicmembership', array('fin_types' => $defaults));
      $form->setDefaults($defaults);
    }
    if ($formName == 'CRM_Contribute_Form_Contribution_Main' && $form->getVar('_id') == 57) {
      CRM_Core_Resources::singleton()->addVars('raicmembership', array('finTypes' => $finTypes));
      // user has a current membership
      if ($initial_membership['status_id'] == 1 || $initial_membership['status_id'] == 2) {
        //TODO create page to redirect to if member is current
        $url = CRM_Utils_System::url('raic/your-membership-current');
        drupal_set_message(t('Please be advised that RAIC memberships can only be renewed for the current year. We will advise you at the start of the next renewal season when your payment is due again. Thank you.'), 'error');
        // CRM_Utils_System::redirect($url);
      }
      // user does not have a "next" membereship
      if (empty($allowed_memberships)) {
        global $user;
        $full_url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        watchdog(
          'raic_members',
          "Memberships: Entry url: %url\nCould not find a membership for Drupal user: %uid \n Contact id: %cid \nCurrent Memberships: %members \nInitial: %initial",
          array(
            '%url' => $full_url,
            '%uid' => $user->uid,
            '%cid' => $contact['id'],
            '%members' => var_export($current_memberships, TRUE),
            '%initial' => $initial_membership,
          )
        );
        // create url to redirect to is user does not have an allowed membership... possibly login page?
        $url = CRM_Utils_System::url('raic/no-membership-renew-found');
        // CRM_Utils_System::redirect($url);
        drupal_set_message(t('We could not find a membership level for you based on your request. Please make sure to login before renewing your membership.'), 'error');
      }
    }
  }

  // event registrations to do tax stuff for
  $settings = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_financial_types();
  // Pricesets for event/membership taxes
  $eventPricesets = array();
  if (!empty($settings['eventpagesps'])) {
    $eventPricesets = explode(',', $settings['eventpagesps']);
  }
  $contribPricesets = array();
  if (!empty($settings['contribpagesps'])) {
    $contribPricesets = explode(',', $settings['contribpagesps']);
  }

  // In-person price options that should show regardless of your province.
  $inPersonPriceOptions = array(
    690,
    691,
    700,
    709,
    710,
    711,
  );

  $prefix = FALSE;

  if ($formName == 'CRM_Contribute_Form_Contribution_Main' && in_array($form->getVar('_id'), $contribPricesets)) {
    $prefix = 'payment_';
  }
  if ($formName == 'CRM_Event_Form_Registration_Register' && in_array($form->_priceSetId, $eventPricesets)) {
    $prefix = 'event_';
  }
  if ($prefix) {
    CRM_Core_Resources::singleton()->addVars('raicmembership', array(
      'finTypes' => CRM_Raicmembership_BAO_Raicmembership::_raicmembership_financial_types(),
      'raicprefix' => $prefix,
      'inPersonPriceOptions' => $inPersonPriceOptions,
    ));
    CRM_Core_Resources::singleton()->addScriptFile('com.aghstrategies.raicmembership', 'js/eventTax.js');
    CRM_Core_Resources::singleton()->addStyleFile('com.aghstrategies.raicmembership', 'css/raicmembership.css');
  }

  // AGH #9803 remove text on renewal page that says "Is this not you? do this for someone else"
  if ($formName == 'CRM_Contribute_Form_Contribution_Main' && $form->getVar('_id') == 57) {
    //TODO may not need this css in new version.
    CRM_Core_Resources::singleton()->addStyleFile('com.aghstrategies.raicmembership', 'css/raicmembership.css');
  }
}

/**
 * Dynamically fill options with Membership Types for MEMBERSHIP_APPROVAL_FIELD_ID
 */
function raicmembership_civicrm_fieldOptions($entity, $field, &$options, $params) {
  if ($field == 'custom_' . MEMBERSHIP_APPROVAL_FIELD_ID) {
    try {
      $types = civicrm_api3('MembershipType', 'get', array(
        'is_active' => 1,
        'options' => array('limit' => 50),
      ));
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicmembership')));
    }
    if (!empty($types['values']) && $types['is_error'] == 0) {
      foreach ($types['values'] as $key => $value) {
        $options[$key] = $value['name'];
      }
    }
  }
}

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function raicmembership_civicrm_config(&$config) {
  _raicmembership_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function raicmembership_civicrm_xmlMenu(&$files) {
  _raicmembership_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function raicmembership_civicrm_install() {
  _raicmembership_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function raicmembership_civicrm_postInstall() {
  _raicmembership_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function raicmembership_civicrm_uninstall() {
  _raicmembership_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function raicmembership_civicrm_enable() {
  _raicmembership_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function raicmembership_civicrm_disable() {
  _raicmembership_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function raicmembership_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _raicmembership_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function raicmembership_civicrm_managed(&$entities) {
  _raicmembership_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function raicmembership_civicrm_caseTypes(&$caseTypes) {
  _raicmembership_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function raicmembership_civicrm_angularModules(&$angularModules) {
  _raicmembership_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_SettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function raicmembership_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _raicmembership_civix_civicrm_alterSettingsFolders($metaDataFolders);
}
