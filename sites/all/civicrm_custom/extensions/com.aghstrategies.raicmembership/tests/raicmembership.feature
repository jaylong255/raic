Feature: RAIC Membereship Renewal - allowed memberships

Scenario: A Member with an Expired membership goes to renew

When on the RAIC membership Renewal page (contribution page id 53)
And the user has an expired membership of <type>
And the user has been a member since <timePassedInYears>
And the Manual Override field is not populated
Then the membership option presented will be of <allowedMembershipTypes>

| Current Membership Type                                          | TimePassed  | Allowed Membership Types 	                                    |
| Graduate from a Canadian School of Architecture                  | N/A year(s) | Graduate from a Canadian School of Architecture	                |
| Graduate from a Canadian School of Architecture (FREE)	       | N/A year(s) | Graduate from a Canadian School of Architecture (FREE)	        |
| Graduate from a Canadian School of Architecture (Initial 3 year) | 1 year(s)	 | Graduate from a Canadian School of Architecture (Initial 3 year) |
| Graduate from a Canadian School of Architecture (Initial 3 year) | 2 year(s)	 | Graduate from a Canadian School of Architecture (Initial 3 year)	|
| Graduate from a Canadian School of Architecture (Initial 3 year) | 3 year(s)	 | Graduate from a Canadian School of Architecture	                |
| Intern / Intern Architect (Initial 5 years)	                   | 1 year(s)	 | Intern / Intern Architect (Initial 5 years)	                    |
| Intern / Intern Architect (Initial 5 years)	                   | 2 year(s)	 | Intern / Intern Architect (Initial 5 years)	                    |
| Intern / Intern Architect (Initial 5 years)	                   | 3 year(s)   | Intern / Intern Architect (Initial 5 years)	                    |
| Intern / Intern Architect (Initial 5 years)	                   | 4 year(s)	 | Intern / Intern Architect (Initial 5 years)	                    |
| Intern / Intern Architect (Initial 5 years)	                   | 5 year(s)   | Intern / Intern Architect	                                    |
| Intern / Intern Architect	                                       | N/A year(s) | Intern / Intern Architect	                                    |
| Graduate from a foreign School of Architecture (1st year)	       | 1 year(s)   | Graduate from a foreign School of Architecture	                |
| Graduate from a foreign School of Architecture	               | N/A year(s) | Graduate from a foreign School of Architecture	                |
| International Associate (1st year)	                           | 1 year(s)   | International Associate	                                        |
| International Associate	                                       | N/A year(s) | International Associate	                                        |
| Registered / Licensed Architect (1st year)	                   | 1 year(s)	 | Registered / Licensed Architect	                                |
| Registered / Licensed Architect (1st year promo)	               | 1 year(s)	 | Registered / Licensed Architect	                                |
| Registered / Licensed Architect	                               | N/A year(s) | Registered / Licensed Architect	                                |
| Affiliate: Institutional                                         | N/A year(s) | Affiliate: Institutional	                                        |
| Affiliate: Individual	                                           | N/A year(s) | Affiliate: Individual	                                        |
| Affiliate: Firms/Corp ($500,000)                                 | N/A year(s) | Affiliate: Firms/Corp ($500,000)	                                |
| Affiliate: Firms/Corp ($500,000 - $2.5M)	                       | N/A year(s) | Affiliate: Firms/Corp ($500,000 - $2.5M)	                        |
| Affiliate: Firms/Corp ($2.5M - $10M)	                           | N/A year(s) | Affiliate: Firms/Corp ($2.5M - $10M)	                            |
| Affiliate: Firms/Corp ($10M - $25M)	                           | N/A year(s) | Affiliate: Firms/Corp ($10M - $25M)                              |
| Affiliate: Firms/Corp (over $25M)                                | N/A year(s) | Affiliate: Firms/Corp (over $25M)                                |
| Faculty, School of Architecture (1st year)                       | 1 year(s)	 | Faculty, School of Architecture	                                |
| Faculty, School of Architecture                                  | N/A year(s) | Faculty, School of Architecture	                                |
| Standard: Retired                                                | N/A year(s) | Standard: Retired	                                            |
| Fellow: Regular	                                               | N/A year(s) | Fellow: Regular	                                                |
| Fellow: Retired                                                  | N/A year(s) | Fellow: Retired	                                                |

Scenario: A Member without a membership goes to renewal page (front end)

When a user is logged in
And has no memberships of any kind
And the Manual Override field is not populated
And visits the RAIC membership Renewal page (contribution page id 53)
Then the user will be redirected to https://raic.org/raic/no-membership-renew-found

Scenario: A Member with a current membership goes to the renewal page (front end)

When a user is logged in
And has a current memberships
And visits the RAIC membership Renewal page (contribution page id 53)
Then the user will be redirected to raic/your-membership-current

Feature: Taxes on membership renewals (front end)

Scenario: A Member with an expired membership and a billing address goes to renew (On the front end)

When a logged in user with an allowed membership visits the RAIC membership Renewal page (contribution page id 53)
And they have a billing address with <Province>
Then the tax (financial type used which should have a sales tax account with the appropriate amount) applied to their membership will match the membership: <province> setting on this page: https://aghdev.raic.org/civicrm/taxessettings
And if they change the their billing address province the tax on the membership should update accordingly.

Scenario: A Member with an expired membership and a billing address goes to renew (On the front end)

When a logged in user with an allowed membership visits the RAIC membership Renewal page (contribution page id 53)
And they have a billing address not in canada
Then the tax (financial type used which should have a sales tax account with the appropriate amount) applied to their membership will match the membership: default setting on this page: https://aghdev.raic.org/civicrm/taxessettings
And if they change the their billing address province the tax on the membership should update accordingly.

Scenario: An admin goes to renew a membership on the backend

When an admin goes to renew with a credit card for a membership on the backend
And for a contact that has a billing address in canada
Then the financial type and membership should default to the financial type for the contacts province
And the membership type should default to the allowed membership based on the logic in the table used on the front end.

Scenario: Using price set 87 (webinars with taxes)

When a user goes to register for an event on the front end
And that event uses price set 87
And That user has an address in civi in canada
Then the taxes applied to the amount will correspond to the event: <province> settings on  https://aghdev.raic.org/civicrm/taxessettings
