### This Extension:

This extension holds the majority of RAIC's custom Membership and Custom Taxes code.

+ Removes yellow box that says Welcome <displayName>. (Not <displayName>, or want to do this for a different person?) at the top of [Membership Renewal page](https://raic.org/civicrm/contribute/transact?reset=1&id=57)
+ for offline membership renewals adds asauve@raic.org as a cc to the receipt.
+ Sets up rules for membership renewal, what memberships the logged in user is allowed to renew
+ sets default when renewing on the backend
+ set up settings page to match provinces to the financial type to use (for events, payments(contributions) and memberships):  https://raic.org/civicrm/taxessettings
+ populates the field options for custom_42 Manual Approval field for membership types
+ On Webinar with taxes event sets up tax code: https://aghdev.raic.org/civicrm/event/register?reset=1&id=80
+ On Submissions for Governor General's Medals in Architecture 2017 contribution page sets up taxes https://aghdev.raic.org/civicrm/contribute/transact?reset=1&id=55
+ On membership renewal page https://raic.org/civicrm/contribute/transact?reset=1&id=57 sets up taxes and when renewing memberships on the backend
+ Adds Angie as a cc and aj as a bcc when A receipt is sent for an offline event registration or a receipt is sent for a payment being recorded

#### Set up:

1. Install and enable the extension
2. Create financial accounts for each province
3. Create financial types for each tax amount
4. Go to civicrm/taxessettings and match each province to the financial account to use
5. Create a price set with all the membership types, for each membership type have an option for each financial type
6. Create pages to redirect to if the logged in user goes to the renewal page and does not have an allowed membership or has a current membership

#### The Custom Tax Code for Events and Contrib Pages:

The Custom Tax Code is applied to the price sets listed in the fields: "Contribution Pages Price Sets" and "Event Pages Price Sets" on the Taxes Settings Page which one can access from the Civicrm Administration Bar -> Administer -> Custom Taxes Settings

The custom tax code expects each field in each of the price sets listed in the "Event Pages Price Sets" field to have 4 price options one for each of the following financial types:

1. Event Fees - GST
2. Event Fees - HST
3. Event Fees - Ontario
4. Event Fees - Quebec

Example Price Set: https://www.raic.org/civicrm/admin/price/field/option?action=browse&reset=1&sid=119&fid=186

It will then filter the option for each price field based on the billing province field on the form (which should be populated based on the billing address on the civicontact of the logged in user and update when the user changes their province field in the billing section of the form).