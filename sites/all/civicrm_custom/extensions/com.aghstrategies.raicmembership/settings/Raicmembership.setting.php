<?php
/**
 * @file
 * Settings metadata for com.aghstrategies.proratemembership.
 * Copyright (C) 2016, AGH Strategies, LLC <info@aghstrategies.com>
 * Licensed under the GNU Affero Public License 3.0 (see LICENSE.txt)
 */
return array(
  'raicmembership_provincetaxes' => array(
    'group_name' => 'Province Taxes Matching',
    'group' => 'raicmembership',
    'name' => 'raicmembership_provincetaxes',
    'type' => 'Array',
    'default' => NULL,
    'add' => '4.6',
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => 'Array of Canadian Provinces and the Financial Types to use for them for Membership Dues',
    'help_text' => 'this setting is used by com.aghstrategies.raicmembership to calculate taxes for members',
  ),
);
