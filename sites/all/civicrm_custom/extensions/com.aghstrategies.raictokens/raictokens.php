<?php

require_once 'raictokens.civix.php';

/**
 * Creates custom tokens for RAIC
 * based on http://forum.civicrm.org/index.php?topic=15784.0
 */
function raictokens_civicrm_tokens(&$tokens) {
  $tokens['raictoken'] = array(
    'raictoken.membershipLatestFee' => 'RAIC memberships: Latest Fee',
    'raictoken.membershipEndDate' => 'RAIC memberships: Latest End Date',
    'raictoken.membershipFeeTaxInfo' => 'RAIC memberships: Fee, Tax Info',
    'raictoken.membershipFeeTotalTaxAmount' => 'RAIC memberships: Fee, Total Tax Amount',
    'raictoken.membershipFeeTotalPayable' => 'RAIC memberships: Fee, Total Payable',
    'raictoken.membershipCurrentName' => 'RAIC memberships: Current Membership Type Name',
    'raictoken.membershipAllowedName' => 'RAIC memberships: Next Allowed Membership Type Name',
    'raictoken.optionalEmployerName' => 'RAIC contact: Employer Name only if Primary = Work',
    'raictoken.contributionTotal' => 'RAIC contributions: Total',
    'raictoken.contributionLast' => 'RAIC contributions: Last Date',
    'raictoken.contributionList' => 'RAIC contributions: List',
    'raictoken.directoryStreet' => 'RAIC member directory: Street Address',
    'raictoken.directorySupp1' => 'RAIC member directory: Additional Address 1',
    'raictoken.directorySupp2' => 'RAIC member directory: Additional Address 2',
    'raictoken.directoryCity' => 'RAIC member directory: City',
    'raictoken.directoryProvince' => 'RAIC member directory: Province',
    'raictoken.directoryCountry' => 'RAIC member directory: Country',
    'raictoken.directoryPostal' => 'RAIC member directory: Postal Code',
    'raictoken.directoryPhone' => 'RAIC member directory: Phone',
    'raictoken.directoryFax' => 'RAIC member directory: Fax',
    'raictoken.directoryEmail' => 'RAIC member directory: Email',
  );
}

function raictokens_civicrm_tokenValues(&$values, &$contactIDs, $jobID) {
  // We could be given an array of contact IDs or a string
  if (is_array($contactIDs)) {
    foreach ($contactIDs as $contactID) {
      $value =& $values[$contactID];
      $value = get_contribution_details($contactID, $value);
      $value = get_optional_employer($contactID, $value);
      $value = get_contribution_totals($contactID, $value);
      $value = get_membership_details($contactID, $value);
      $value = get_memberdirectory_address($contactID, $value);
    }
  }
  else {
    $value =& $values;
    $value = get_contribution_details($contactIDs, $value);
    $value = get_optional_employer($contactIDs, $value);
    $value = get_contribution_totals($contactIDs, $value);
    $value = get_membership_details($contactIDs, $value);
    $value = get_memberdirectory_address($contactIDs, $value);
  }
}

function get_membership_details($contact_id, &$value) {
  $membership_latest_fee = $membership_end_date = $taxAmount = 0;
  $allowed_membership_type = $tax_info = array();
  $contact = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_contact($contact_id);
  $current_memberships = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_current_membership_for_contact($contact);
  $initial_membership = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_initial_membership($current_memberships);
  // TODO: Allowed memberships is just an array of ids, might be better if it loaded more data?
  $allowed_memberships = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_get_next_membership_for_contact($contact, $initial_membership);
  if (is_array($initial_membership) && isset($initial_membership['membership_end_date'])) {
    $membership_end_date = $initial_membership['membership_end_date'];
  }
  if (is_array($allowed_memberships) && !empty($allowed_memberships)) {
    if (count($allowed_memberships) > 1) {
      // TODO: Issue warning
    }
    $allowed_membership_type_id = $allowed_memberships[0];
    if (-1 == $allowed_membership_type_id) {
      // TODO: Issue warning (fallback type)
    }
    else {
      $params = array(
        'id' => $allowed_membership_type_id,
      );
      try {
        $type_results = civicrm_api3('MembershipType', 'get', $params);
      }
      catch (CiviCRM_API3_Exception $e) {
        $error = $e->getMessage();
        CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
      }
      if ($type_results['is_error'] !== 1) {
        $allowed_membership_type = $type_results['values'][$allowed_membership_type_id];
      }
    }
  }
  if (isset($allowed_membership_type['minimum_fee'])) {
    $membership_latest_fee = $allowed_membership_type['minimum_fee'];
  }
  //TODO rewrite tax logic
  // rewrite taxes tokens values to use these numbers
  $taxRate = raictokens_get_financial_type_for_contact($contact);
  $taxAmount = CRM_Contribute_BAO_Contribution_Utils::calculateTaxAmount($membership_latest_fee, $taxRate['tax_rate']);
  $tax_info[] = 'Sales Tax Rate for ' . $taxRate['province'] . '(' . number_format($taxRate['tax_rate'], 3) . '%)';
  $membership_tax_info = implode(' + ', $tax_info);
  $membership_tax_info .= ' = $' . number_format($taxAmount['tax_amount'], 2, '.', ' ');
  $value['raictoken.membershipLatestFee'] = number_format(round($membership_latest_fee, 2), 2, '.', ' ');
  $value['raictoken.membershipEndDate'] = CRM_Utils_Date::customFormat($membership_end_date, NULL, array('M', 'd', 'Y'));
  $value['raictoken.membershipFeeTaxInfo'] = $membership_tax_info;
  $value['raictoken.membershipFeeTotalTaxAmount'] = number_format(round($taxAmount['tax_amount'], 2), 2, '.', ' ');
  $value['raictoken.membershipFeeTotalPayable'] = number_format(round($membership_latest_fee + $taxAmount['tax_amount'], 2), 2, '.', ' ');
  $value['raictoken.membershipCurrentName'] = (is_array($initial_membership) && isset($initial_membership['membership_name'])) ?
    $initial_membership['membership_name'] : '';
  $value['raictoken.membershipAllowedName'] = (isset($allowed_membership_type['name'])) ?
    $allowed_membership_type['name'] : '';

  return $value;
}

function get_optional_employer($contact_id, &$value) {
  $is_work_primary  = FALSE;
  $employer = '';
  $params = array(
    'sequential' => 1,
    'contact_id' => $contact_id,
    // Work
    'location_type_id' => 2,
  );
  try {
    $address_results = civicrm_api3('Address', 'get', $params);
  }
  catch (CiviCRM_API3_Exception $e) {
    $error = $e->getMessage();
    CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
  }
  if ($address_results['is_error'] !== 1) {
    $work_address = $address_results['values'][0];
    if (isset($work_address['is_primary']) && $work_address['is_primary'] == 1) {
      $is_work_primary = TRUE;
    }
  }
  if ($is_work_primary) {
    // Get the relationship
    $params = array(
      'sequential' => 1,
      'contact_id_a' => $contact_id,
      // Work
      'relationship_type_id' => 4,
    );
    try {
      $employer_results = civicrm_api3('Relationship', 'get', $params);
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
    }
    $employer_contact_id = NULL;
    if ($employer_results['is_error'] !== 1) {
      foreach ($employer_results['values'] as $result) {
        if (isset($result['is_active']) && $result['is_active'] == 1) {
          $employer_contact_id = $result['contact_id_b'];
        }
      }
      if (isset($employer_contact_id)) {
        $params = array(
          'id' => $employer_contact_id,
        );
        try {
          $employer_contact_results = civicrm_api3('Contact', 'get', $params);
        }
        catch (CiviCRM_API3_Exception $e) {
          $error = $e->getMessage();
          CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
        }
        if ($employer_contact_results['is_error'] !== 1) {
          $employer_contact = $employer_contact_results['values'][$employer_contact_id];
          if (isset($employer_contact['organization_name'])) {
            $employer = $employer_contact['organization_name'] . '<br/>';
          }
        }
      }
    }
  }
  $value['raictoken.optionalEmployerName'] = $employer;
  return $value;
}

function get_contribution_totals($cid, &$value) {
  $query = "
    SELECT sum( total_amount ) as total_amount,
           contact_id,
           max( receive_date ) as receive_date
    FROM   civicrm_contribution
    WHERE  contact_id = ( $cid )
    AND    is_test = 0
    GROUP BY contact_id
    ";

  $dao = CRM_Core_DAO::executeQuery($query);
  while ($dao->fetch()) {
    $value['raictoken.contributionTotal'] = $dao->total_amount;
    $value['raictoken.contributionLast'] = CRM_Utils_Date::customFormat($dao->receive_date, NULL, array('M', 'd', 'Y'));
  }
  return $value;
}

function get_contribution_details($cid, &$value) {
  $params = array(
    'sort' => 'receive_date DESC',
    'contact_id' => $cid,
    'limit' => 5,
  );

  try {
    $contributions = civicrm_api3('Contribution', 'get', $params);
  }
  catch (CiviCRM_API3_Exception $e) {
    $error = $e->getMessage();
    CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
  }
  $i = 0;
  $value['raictoken.contributionList'] = "<table><tr><th>Amount</th><th>Date</th><th>Status</th><th>Type</th></tr>";
  if ($contributions['is_error'] == 0) {
    foreach ($contributions['values'] as $contribution) {
      if ($i < 5) {
        $value['raictoken.contributionList'] .= "<tr><td>$" . $contribution['total_amount'] . "</td>";
        $value['raictoken.contributionList'] .= "<td>" . CRM_Utils_Date::customFormat($contribution['receive_date'], NULL, array('M', 'd', 'Y')) . "</td>";
        $value['raictoken.contributionList'] .= "<td>" . $contribution['contribution_status_id'] . "</td>";
        $value['raictoken.contributionList'] .= "<td>" . $contribution['financial_type'] . "</td></tr>";
      }
      $i++;
    }
  }
  $value['raictoken.contributionList'] .= "</table>";
  return $value;
}

function get_memberdirectory_address($cid, &$value) {
  $token_names = array(
    'directoryStreet' => '',
    'directorySupp1' => '',
    'directorySupp2' => '',
    'directoryCity' => '',
    'directoryProvince' => '',
    'directoryCountry' => '',
    'directoryPostal' => '',
    'directoryPhone' => '',
    'directoryFax' => '',
    'directoryEmail' => '',
  );
  try {
    $directory_address = civicrm_api3("Address", "get", array(
      'location_type_id' => '7',
      'contact_id' => $cid,
    ));
  }
  catch (CiviCRM_API3_Exception $e) {
    $error = $e->getMessage();
    CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
  }
  $v = array();
  if ($directory_address['is_error'] == 0 && $directory_address['count'] > 0) {
    $values = $directory_address['values'];
    $v = array_shift($values);
  }
  $token_names['directoryStreet'] = isset($v['street_address']) ? $v['street_address'] : '';
  $token_names['directorySupp1'] = isset($v['supplemental_address_1']) ? $v['supplemental_address_1'] : '';
  $token_names['directorySupp2'] = isset($v['supplemental_address_2']) ? $v['supplemental_address_2'] : '';
  $token_names['directoryCity'] = isset($v['city']) ? $v['city'] : '';
  $token_names['directoryPostal'] = isset($v['postal_code']) ? $v['postal_code'] : '';
  $token_names['directoryProvince'] = isset($v['state_province_id']) ? $v['state_province_id'] : '';
  $token_names['directoryCountry'] = isset($v['country_id']) ? $v['country_id'] : '';

  $token_names['directoryCountry'] = intval($token_names['directoryCountry']) ?
    CRM_Core_PseudoConstant::country($token_names['directoryCountry']) : '';
  $token_names['directoryProvince'] = intval($token_names['directoryProvince']) ?
    CRM_Core_PseudoConstant::stateProvince($token_names['directoryProvince']) : '';

  try {
    $email_results = civicrm_api3("Email", "get", array(
      'location_type_id' => '7',
      'contact_id' => $cid,
    ));
  }
  catch (CiviCRM_API3_Exception $e) {
    $error = $e->getMessage();
    CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
  }
  foreach ($email_results['values'] as $k => $v) {
    $token_names['directoryEmail'] = $v['email'];
  }

  try {
    $phone_results = civicrm_api3("Phone", "get", array(
      'location_type_id' => '7',
      'contact_id' => $cid,
    ));
  }
  catch (CiviCRM_API3_Exception $e) {
    $error = $e->getMessage();
    CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
  }
  // phone = 1, fax = 3
  foreach ($phone_results['values'] as $k => $v) {
    if ($v['phone_type_id'] == 1) {
      $token_names['directoryPhone'] = $v['phone'];
    }
    if ($v['phone_type_id'] == 3) {
      $token_names['directoryFax'] = $v['phone'];
    }
  }

  foreach ($token_names as $key => $token_value) {
    $value['raictoken.' . $key] = $token_value;
  }
  return $value;
}

function raictokens_get_financial_type_for_contact($contact) {
  $taxRate = array(
    'tax_rate' => 0,
    'tax_label' => 'not found',
  );
  // check if user has Province
  if (!empty($contact['state_province_id'])) {
    try {
      $province = civicrm_api3('StateProvince', 'getsingle', array(
        'sequential' => 1,
        'id' => $contact['state_province_id'],
      ));
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
    }
    $finTypes = CRM_Raicmembership_BAO_Raicmembership::_raicmembership_financial_types();
    $finTypeToUse = $finTypes['membership_default'];
    if (!empty($finTypes["membership_{$contact['state_province_id']}"])) {
      $finTypeToUse = $finTypes["membership_{$contact['state_province_id']}"];
    }
    try {
      $taxAccount = civicrm_api3('EntityFinancialAccount', 'get', array(
        'sequential' => 1,
        'account_relationship' => "Sales Tax Account is",
        'entity_table' => "civicrm_financial_type",
        'entity_id' => $finTypeToUse,
        'api.FinancialAccount.getsingle' => array(
          'id' => "\$value.financial_account_id",
          'is_active' => 1,
          'is_tax' => 1,
        ),
      ));
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raictokens')));
    }
    if (!empty($taxAccount['values'][0]['api.FinancialAccount.getsingle']['tax_rate'])) {
      $taxRate['fin_type'] = $finTypeToUse;
      $taxRate['tax_rate'] = $taxAccount['values'][0]['api.FinancialAccount.getsingle']['tax_rate'];
      $taxRate['tax_label'] = $taxAccount['values'][0]['api.FinancialAccount.getsingle']['name'];
      $taxRate['province'] = $province['name'];
    }
    return $taxRate;
  }
}

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function raictokens_civicrm_config(&$config) {
  _raictokens_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function raictokens_civicrm_xmlMenu(&$files) {
  _raictokens_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function raictokens_civicrm_install() {
  _raictokens_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function raictokens_civicrm_postInstall() {
  _raictokens_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function raictokens_civicrm_uninstall() {
  _raictokens_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function raictokens_civicrm_enable() {
  _raictokens_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function raictokens_civicrm_disable() {
  _raictokens_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function raictokens_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _raictokens_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function raictokens_civicrm_managed(&$entities) {
  _raictokens_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function raictokens_civicrm_caseTypes(&$caseTypes) {
  _raictokens_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function raictokens_civicrm_angularModules(&$angularModules) {
  _raictokens_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function raictokens_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _raictokens_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function raictokens_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function raictokens_civicrm_navigationMenu(&$menu) {
  _raictokens_civix_insert_navigation_menu($menu, NULL, array(
    'label' => ts('The Page', array('domain' => 'com.aghstrategies.raictokens')),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _raictokens_civix_navigationMenu($menu);
} // */
