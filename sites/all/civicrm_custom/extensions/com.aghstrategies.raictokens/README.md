### This Custom Module:

Creates the following civiMail tokens:

'RAIC memberships: Latest Fee',  
'RAIC memberships: Latest End Date',  
'RAIC memberships: Fee, Tax Info',  
'RAIC memberships: Fee, Total Tax Amount',  
'RAIC memberships: Fee, Total Payable',  
'RAIC memberships: Current Membership Type Name',  
'RAIC memberships: Next Allowed Membership Type Name',  
'RAIC contact: Employer Name only if Primary = Work',  
'RAIC contributions: Total',  
'RAIC contributions: Last Date',  
'RAIC contributions: List',  
'RAIC member directory: Street Address',  
'RAIC member directory: Additional Address 1',  
'RAIC member directory: Additional Address 2',  
'RAIC member directory: City',  
'RAIC member directory: Province',  
'RAIC member directory: Country',  
'RAIC member directory: Postal Code',  
'RAIC member directory: Phone',  
'RAIC member directory: Fax',  
'RAIC member directory: Email',  

#### To Test:

For a contact with a membership who shows up in the member directory create an [email thru civiMail](http://aghdev.raic.org/civicrm/activity/email/add?atype=3&action=add&reset=1&context=standalone) using these tokens and send to yourself.

#### Examples of Mailing Templates that use these tokens:

https://raic.org/civicrm/admin/messageTemplates/add?action=update&id=52&reset=1  
https://raic.org/civicrm/admin/messageTemplates/add?action=update&id=51&reset=1
