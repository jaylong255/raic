<?php

require_once 'raicdirectory.civix.php';

function raicdirectory_civicrm_postProcess($formName, &$form) {
  if (is_a($form, 'CRM_Profile_Form_Edit')) {
    //drupal_set_message('profile edit');
    $gid = $form->getVar('_gid');
    $cid = $form->getVar('_id');
    // 26 is "update my contact" and 29 is the Member Profile Update ID
    if ($gid == 29 || $gid == 26) {
      $submissions = $form->exportValues();
      _rd_clear_fields($cid, $submissions);
      if (!empty($submissions['custom_26'])) {
        $custom_26_int = (int) $submissions['custom_26'];
        _rd_update_core_address_selection($cid, $custom_26_int);
      }
    }
  }
  elseif (is_a($form, 'CRM_Contact_Form_Contact') || is_a($form, 'CRM_Contact_Form_Inline_Address')) {
    $submissions = $form->exportValues();
    //drupal_set_message('backend update contact: '. $formName . implode(', ', array_keys($submissions)).'<br />'. implode(', ', array_values($submissions)));
    $custom_location_type = $core_location_type = 0;

    $cid = $form->getVar('_contactId');

    $core_location_type = _rd_get_primary_address_type($submissions);
    $custom_addr_field_name = _rd_find_custom($submissions, 'custom_26');

    if (!empty($submissions[$custom_addr_field_name])) {
      $custom_location_type = (int) $submissions[$custom_addr_field_name];
    }
    //drupal_set_message('backend update custom: '.$custom_location_type.' core: '.$core_location_type);

    // If $core_location_type == 0 something has gone wrong. Fail silently.
    // If we are only setting $core_location_type then update the custom field
    // Other possible cases are:
    // ($core_location_type != 0 && $core_location_type == $custom_location_type) -- Her we have nothing to do
    // ($core_location_type != 0 && $core_location_type != $custom_location_type) -- This is a job for form validation, tell the user to fix.
    if (($core_location_type != 0) && ($custom_location_type == 0)) {
      $params = array('custom_26' => $core_location_type, 'version' => 3, 'entity_id' => $cid);
      try {
        $result = civicrm_api3('custom_value', 'create', $params);
      }
      catch (CiviCRM_API3_Exception $e) {
        $error = $e->getMessage();
        CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicdirectory')));
      }
    }
  }
}


function raicdirectory_civicrm_validate($formName, &$fields, &$files, &$form) {
  $errors = array();
  //drupal_set_message('validating  '. $formName . '<pre>'. implode(',', array_keys($fields)).'</pre>');
  if ($formName == 'CRM_Contact_Form_Contact' || $formName == 'CRM_Contact_Form_Inline_Address') {
    $custom_location_field = _rd_find_custom($fields, 'custom_26');
    $custom_location_type = CRM_Utils_Array::value($custom_location_field, $fields);
    $core_location_type = _rd_get_primary_address_type($fields);
    //drupal_set_message('validating custom: '.$custom_location_type.' core: '.$core_location_type);
    if (!empty($custom_location_type) && ($custom_location_type != $core_location_type)) {
      $errors[$custom_location_field] = ts("If setting both Primary Address and Preferred Location they must match.");
    }
    $allowed_types = _rd_get_allowed_location_types();
    // TODO: This should be on a different field. On inline editing the error cannot be seen and submission fails silently.
    if (array_key_exists($core_location_type, $allowed_types) === FALSE) {
      $errors[$custom_location_field] = ts("Please check Primary Location Checkbox. Primary Location can only be Work or Home.");
    }
  }
  //return empty($errors) ? true : $errors; //As per wiki
  // See http://forum.civicrm.org/index.php?topic=24098.0
  return empty($errors) ? TRUE : $errors;
}


function _rd_update_core_address_selection($cid, $address_type_id) {
  $params = array('version' => 3, 'contact_id' => $cid);
  try {
    $existing = civicrm_api3('Address', 'get', $params);
  }
  catch (CiviCRM_API3_Exception $e) {
    $error = $e->getMessage();
    CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicdirectory')));
  }
  //TODO this if does nothing
  if ($address_type_id == 1 || $address_type_id == 2) {

  }
  $old_primary_id = 0;
  $new_primary_id = 0;
  foreach ($existing['values'] as $address_id => $address) {
    $old_primary = ($address['is_primary'] == 1) ? $address_id : $old_primary_id;
  }

  foreach ($existing['values'] as $address_id => $address) {
    if ($address['location_type_id'] == $address_type_id) {
      $new_primary_id = $address_id;
    }
  }
  if ($new_primary_id != 0 && ($new_primary_id != $old_primary_id)) {
    try {
      $results = civicrm_api3('Address', 'update', array('version' => 3, 'id' => $new_primary_id, 'is_primary' => 1));
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicdirectory')));
    }
  }
}

function _rd_get_primary_address_type($submissions) {
  //ddebug_backtrace();
  $core_location_type = 0;
  foreach ($submissions['address'] as $addr) {
    //drupal_set_message('backend update getting primary: '. implode(', ', array_keys($addr)). implode(', ', array_values($addr)));
    if (isset($addr['is_primary']) && $addr['is_primary'] == 1) {
      $core_location_type = $addr['location_type_id'];
    }
  }
  return $core_location_type;
}

// Needed since the custom fields are stored in $submissions as 'custom_26_7689' and so on
/**
 * [_rd_find_custom description]
 * @param  [type] $submissions       [description]
 * @param  [type] $custom_field_name [description]
 * @return [type]                    [description]
 */
function _rd_find_custom($submissions, $custom_field_name) {
  $custom_field_form_name = '';
  foreach ($submissions as $key => $notusedvalue) {
    if (is_int(strpos($key, 'custom_26'))) {
      $custom_field_form_name = $key;
    }
  }
  return $custom_field_form_name;
}

function _rd_get_allowed_location_types() {
  return array(1 => t('Home'), 2 => t('Work'));
}

function _rd_clear_fields($contact_id, $submissions) {
  // Handle phone, fax, email and website
  $fields_to_clear = array(
    'phone-1-1' => array('Phone', 1, 1),
    'phone-1-3' => array('Phone', 1, 3),
    'phone-2-1' => array('Phone', 2, 1),
    'phone-2-3' => array('Phone', 2, 3),
    'phone-7-1' => array('Phone', 7, 1),
    'phone-7-3' => array('Phone', 7, 3),
    'email-1' => array('Email', 1, NULL),
    'email-2' => array('Email', 2, NULL),
    'email-7' => array('Email', 7, NULL),
    'url-1' => array('Website', 6, NULL),
  );

  foreach ($fields_to_clear as $field_name => $field_type) {
    $find_address_params = array(
      'version' => 3,
      'contact_id' => $contact_id,
    );
    // If the field has a blank value
    if (isset($submissions[$field_name]) && $submissions[$field_name] == '') {
      if ($field_type[0] == 'Phone') {
        $find_address_params['location_type_id'] = $field_type[1];
        $find_address_params['phone_type_id'] = $field_type[2];
      }
      elseif ($field_type[0] == 'Email') {
        $find_address_params['location_type_id'] = $field_type[1];
      }
      elseif ($field_type[0] == 'Website') {
        $find_address_params['website_type_id'] = $field_type[1];
      }
      try {
        $get_results = civicrm_api3($field_type[0], 'get', $find_address_params);
      }
      catch (CiviCRM_API3_Exception $e) {
        $error = $e->getMessage();
        CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicdirectory')));
      }
      if ($get_results['is_error'] !== 1) {
        foreach ($get_results['values'] as $found_record) {
          $delete_params  = array('version' => 3, 'id' => $found_record['id']);
          try {
            $results = civicrm_api3($field_type[0], 'delete', $delete_params);
          }
          catch (CiviCRM_API3_Exception $e) {
            $error = $e->getMessage();
            CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicdirectory')));
          }
        }
      }
    }
  }
  // Handle address issue with state_province_id and country_id
  $address_fields = array(
    'city-1' => array('city', 1),
    'city-2' => array('city', 2),
    'city-7' => array('city', 7),
    'city-Primary' => array('city', 'is_primary'),
    'country-1' => array('country_id', 1),
    'country-2' => array('country_id', 2),
    'country-7' => array('country_id', 7),
    'country-Primary' => array('country_id', 'is_primary'),
    'postal_code-1' => array('postal_code', 1),
    'postal_code-2' => array('postal_code', 2),
    'postal_code-7' => array('postal_code', 7),
    'postal_code-Primary' => array('postal_code', 'is_primary'),
    'state_province-1' => array('state_province_id', 1),
    'state_province-2' => array('state_province_id', 2),
    'state_province-7' => array('state_province_id', 7),
    'state_province-Primary' => array('state_province_id', 'is_primary'),
    'street_address-1' => array('street_address', 1),
    'street_address-2' => array('street_address', 2),
    'street_address-7' => array('street_address', 7),
    'street_address-Primary' => array('street_address', 'is_primary'),
  );

  $update_params = array(
    1 => array('version' => 3),
    2 => array('version' => 3),
    7 => array('version' => 3),
    'is_primary' => array('version' => 3),
  );
  $find_address_params = array(
    1 => array('version' => 3, 'contact_id' => $contact_id),
    2 => array('version' => 3, 'contact_id' => $contact_id),
    7 => array('version' => 3, 'contact_id' => $contact_id),
    'is_primary' => array('version' => 3, 'contact_id' => $contact_id),
  );
  $find_list = array();
  foreach ($address_fields as $field_name => $field_record) {
    if (isset($submissions[$field_name]) && $submissions[$field_name] == '') {
      if (is_numeric($field_record[1])) {
        $find_address_params[$field_record[1]]['location_type_id'] = $field_record[1];
      }
      elseif ($field_record[1] == 'is_primary') {
        $find_address_params[$field_record[1]]['is_primary'] = 1;
      }
      // Flag that we will update this address type
      $find_list[$field_record[1]] = $field_record[1];
      // Store the updated field data
      // Trick: Found in line 326 - 342 of CRM_Core_BAO_Address that anything under 1000 = NULL
      if ('state_province_id' == $field_record[0] || 'country_id' == $field_record[0]) {
        // was '1' in 4.3.3 as per above trick;
        $update_params[$field_record[1]][$field_record[0]] = '';
      }
      // Regular text fields
      else {
        $update_params[$field_record[1]][$field_record[0]] = '';
      }
    }
  }
  foreach ($find_list as $update_type) {
    $find_params = $find_address_params[$update_type];
    try {
      $get_results = civicrm_api3('Address', 'get', $find_params);
    }
    catch (CiviCRM_API3_Exception $e) {
      $error = $e->getMessage();
      CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicdirectory')));
      drupal_set_message(t('Could not update all address information. Please contact us for assistance'));
    }

    if ($get_results['is_error'] !== 1) {
      foreach ($get_results['values'] as $found_record) {
        $update_params[$update_type]['id'] = $found_record['id'];
        try {
          $results = civicrm_api3('Address', 'update', $update_params[$update_type]);
        }
        catch (CiviCRM_API3_Exception $e) {
          $error = $e->getMessage();
          CRM_Core_Error::debug_log_message(t('API Error: %1', array(1 => $error, 'domain' => 'com.aghstrategies.raicdirectory')));
          drupal_set_message(t('Could not update all address information. Please contact us for assistance'));
        }
      }
    }
  }
}

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function raicdirectory_civicrm_config(&$config) {
  _raicdirectory_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function raicdirectory_civicrm_xmlMenu(&$files) {
  _raicdirectory_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function raicdirectory_civicrm_install() {
  _raicdirectory_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function raicdirectory_civicrm_postInstall() {
  _raicdirectory_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function raicdirectory_civicrm_uninstall() {
  _raicdirectory_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function raicdirectory_civicrm_enable() {
  _raicdirectory_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function raicdirectory_civicrm_disable() {
  _raicdirectory_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function raicdirectory_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _raicdirectory_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function raicdirectory_civicrm_managed(&$entities) {
  _raicdirectory_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function raicdirectory_civicrm_caseTypes(&$caseTypes) {
  _raicdirectory_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function raicdirectory_civicrm_angularModules(&$angularModules) {
  _raicdirectory_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function raicdirectory_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _raicdirectory_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function raicdirectory_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function raicdirectory_civicrm_navigationMenu(&$menu) {
  _raicdirectory_civix_insert_navigation_menu($menu, NULL, array(
    'label' => ts('The Page', array('domain' => 'com.aghstrategies.raicdirectory')),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _raicdirectory_civix_navigationMenu($menu);
} // */
