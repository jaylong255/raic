### This Extension:

REPLACES the [raic_directory](https://git.aghstrategies.com/RAIC/raic_directory) drupal module

When on a civicrm page with profile 26 -> "update my contact" or 29 -> the Member Profile in edit mode

+ For home, directory, or work location types if the contact has an address, phone, email or website and deletes the information on the profile edit form then the related contact information is deleted. so if the profile loads their home address, and they delete the inputs and click save their home address is deleted.  

+ If the field "Preferred Mailing Address" (custom_26) is filled, looks up if the contact has an existing address of that type (home or work) and makes it the users primary address

When updating a contact on the backend

+ if their primary address is changed to a different location type by an admin custom code sets the field "Preferred Mailing Address" (custom_26) to be the address type choosen if home or work or clears it if other

+ custom validation function for if an admin is updating both the primary address AND custom_26 that makes sure they match and primary location can only be work or home
