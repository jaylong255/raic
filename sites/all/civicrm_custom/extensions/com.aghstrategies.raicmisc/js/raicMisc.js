CRM.$(function ($) {
  $('.crm-section.event_contact-section').insertBefore('.crm-section.event_date_time-section');
  $('.crm-section.event_fees-section').insertBefore('.crm-section.event_date_time-section');
  $('.crm-section.event_description-section.summary').insertAfter('.crm-section.event_map-section');
});
