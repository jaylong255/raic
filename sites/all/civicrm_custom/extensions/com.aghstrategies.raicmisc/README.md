This Extension
--------------

Moves around text on the [Event Info page for event ID 84](https://www.raic.org/civicrm/event/info?id=84&reset=1) as requested by RAIC see screen shot of request from RAIC below:

![RAIC's screenshot with instructions where to move things](img/raicRequest.png)

and screen shot of updated version with this customization (one can also navigate to https://www.raic.org/civicrm/event/info?id=84&reset=1 to see these changes):

![Screenshot of how it should look with this custom code](img/updatedWithThisCustomCode.png)

NOTE: This customization is really only necessary while event ID 84 is active. We can deactivate it when the event Info page for event ID 84 is no longer linked on their site.

WHEN UPGRADING LOOK TO SEE IF EVENT 84 IS STILL ACTIVE IF IT IS NOT THIS CUSTOMIZATION CAN BE REMOVED.
