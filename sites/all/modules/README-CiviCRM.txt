CiviCRM Core is modified as follows:

1)
http://issues.civicrm.org/jira/browse/CRM-14148
https://github.com/eileenmcnaughton/civicrm-core/commit/b1d339fc63ff8e5fae67d51ffdcc7266a3c2287b
CRM/Utils/System/Base.php
- $timeZoneOffset = sprintf("%02d:%02d", $tz / 3600, ($tz/60)%60 );
+ $timeZoneOffset = sprintf("%02d:%02d", $tz / 3600, abs(($tz/60)%60));

2)
Moneris mpgClasses.php is manually added to packages/Services
