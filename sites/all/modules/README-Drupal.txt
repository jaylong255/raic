1. The following patch was applied to the Term Level Field Module.
https://www.drupal.org/node/1257778

2. sites/all/modules/contrib/ubercart/uc_file/uc_file.module was modified as such.
define('UC_FILE_PAGER_SIZE', 100);
it was set at 50. 

3. The following patch was applied to the Internationalization Module.
https://www.drupal.org/files/issues/i18n-node_edit-2250501-8.patch

4. Views Slideshow (specifically, three of the theme files) has been patched to allow better theming of the slideshow controls.

Patch: http://drupal.org/node/1949118#comment-7203504
Blog post with full info: http://tylerfrankenstein.com/code/drupal-theme-previous-next-pause-and-resume-controls-views-slideshow

Note that I had to apply the patch manually, as some of the lines in views_slideshow.theme.inc were no longer in the same position as they had been in the version that the patch was made for.

5. Webform CiviCRM was patches as per https://www.drupal.org/node/2402251
in file webform_civicrm\includes\wf_crm_webform_postprocess.inc
Had to apply the patch manually because the module code has changed and the lines were not exactly as the patch provided.