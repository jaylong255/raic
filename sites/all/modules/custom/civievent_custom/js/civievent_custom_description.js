(function ($) {
Drupal.behaviors.dynamic_shipping = {};
    Drupal.behaviors.dynamic_shipping.attach = function (context, settings) {
    	
	    	jQuery( document ).ready( function() {
	            jQuery( ".page-civicrm-event-manage #page-title" ).append( "<div class='gfee-description' style='font-size: 14px !important; padding-top: 15px; color: #3e3e3e;'></div>" );	            
	    	});
    	
    	  if(location.pathname == '/civicrm/event/manage/fee') {    		  
    			  jQuery('.page-civicrm-event-manage #page-title .gfee-description').html('<p>Below is a section to be used for multiple pricing levels. Use the table to enter descriptive labels and amounts for two event fee levels. The first MUST be the member price level, and the second MUST be the non-member price level. No other entries may be entered.</p><p>The same applies for the discounted price section further down the page. The first MUST be the member price level, and the second MUST be the non-member price level. No other entries may be entered.</p>');   	       
    		  
		  }
    	  
    	  jQuery('.page-civicrm-event-manage .crm-tab-button').click(function(){
    		  if(jQuery('#tab_fee').hasClass('ui-tabs-active')) {
	      		  jQuery('.page-civicrm-event-manage #page-title .gfee-description').html('<p>Below is a section to be used for multiple pricing levels. Use the table to enter descriptive labels and amounts for two event fee levels. The first MUST be the member price level, and the second MUST be the non-member price level. No other entries may be entered.</p><p>The same applies for the discounted price section further down the page. The first MUST be the member price level, and the second MUST be the non-member price level. No other entries may be entered.</p>');   	       
	      		  
	      	  } else {
	      		  jQuery('.page-civicrm-event-manage #page-title .gfee-description').html('');
	      	  }
    		  
    	  });
    	  
    	  jQuery('#tab_fee').click(function(){    		  
			  jQuery('.page-civicrm-event-manage #page-title .gfee-description').html('<p>Below is a section to be used for multiple pricing levels. Use the table to enter descriptive labels and amounts for two event fee levels. The first MUST be the member price level, and the second MUST be the non-member price level. No other entries may be entered.</p><p>The same applies for the discounted price section further down the page. The first MUST be the member price level, and the second MUST be the non-member price level. No other entries may be entered.</p>');   	       
		  
	  });
	  
    	  
    	  
    	  
    	  
        }
})(jQuery);
