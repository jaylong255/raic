This Module
-----------

+ Adds the following links to the Drupal Admin Menu (Under Configuration->Civicrm):
  - Configure RAIC Membership Rules: Configure the rules defining transitions from one RAIC membership type to another.
  - Add New RAIC Membership Rule
  - Edit RAIC Membership Rule
  - RAIC Membership General Settings

+ BROKEN: adds a select of state provinces to the [Financial Account form](https://raic.org/civicrm/admin/financial/financialAccount?action=add&reset=1) for "Apply Tax Rate to Provinces", I think that this is supposed to be a way for admins to update what states the to apply the taxes to.

+ BROKEN override the content of the civicrm membership renewal page if you do not have an active membership or are an early renewal to be the text entered [here](https://raic.org/admin/config/civicrm/raic_members_admin_settings)

+ Manages the ["Membership Manual Override" custom field](https://raic.org/civicrm/admin/custom/group/field?reset=1&action=browse&gid=12) The description of this field is "Use these fields to indicate a manual approval for a new membership type. This will set the Membership Renewal page to prompt the user to choose that Membership type." and the options are all membership types.

+ BROKEN and will be rewritten to use the new civicrm tax functionality and needs to work on front and back end: If on a civicrm contrib form not using priceset [70](https://raic.org/civicrm/admin/price/field?reset=1&action=browse&sid=70) which is the priceset for the conference ... (membership renewal/membership page).

  - Sets default membership if user has an "allowed membership" based on rules set [here](https://raic.org/admin/config/civicrm/config_raic_member_rules) or the "Membership Manual Override" custom field has a value for the logged in user
  - Throws an error if the contact does not have a billing address with a state province id in Canada.
  - If the user does have a state province id in Canada for their billing address and an allowed membership, finds the taxes for the logged in contact based on billing address state_province_id and the financial accounts table below (some users may have to pay the federal and state taxes) and sets the [taxes custom field set, group id 13](https://raic.org/civicrm/admin/custom/group/field?reset=1&action=browse&gid=13) that are included on the form in the profile "Applicable Taxes", values and labels to be the taxes the contact owes and charges the contact accordingly. The taxes charged are saved to the contribution in the custom fields.

+ On install sets up the following [financial accounts](https://raic.org/civicrm/admin/financial/financialAccount?reset=1) and on uninstall... removes:

| Name   | Description              | state province id's                      | tax rate  |
|--------|--------------------------|------------------------------------------|-----------|
| GST    | Canadian Federal GST     | 1100, 1101, 1102, 1105, 1107, 1110, 1111 | 5.0       |
| PST-BC | British Columbia PST     | 1101                                     | 7.0       |
| PST-MB | Manitoba PST             | 1102                                     | 7.0       |
| HST-NB | New Brunswick HST        | 1103                                     | 13.0      |
| HST-NL | Newfoundland HST         | 1104                                     | 13.0      |
| HST-NS | Nova Scotia HST          | 1106                                     | 15.0      |
| HST-ON | Ontario HST              | 1108                                     | 13.0      |
| HST-PE | Prince Edward Island HST | 1109                                     | 14.0      |
| QST-QC | Quebec QST               | 1110                                     | 9.975     |
| PST-SK | Saskatchewan PST         | 1111                                     | 10.0      |
