/**
 * Created with JetBrains PhpStorm.
 * User: lola
 * Date: 30/05/13
 * Time: 3:53 PM
 * To change this template use File | Settings | File Templates.
 */

cj(function($) {
    // Hide total
    //$('#pricesetTotal').hide();
    // TODO: Fix total not recalculating
    // Get setting from php
    var taxes = CRM.raic_members.tax_values;
    var rm_gid = CRM.raic_members.billing_profile_gid;
    // Store the previous value
    var billing_prov_sel = $('select[id^=billing_state_province_id]').first();
    billing_prov_sel.data("prev",billing_prov_sel.val());

    var billing_country_sel = $('select[id^=billing_country_id]').first();
    billing_country_sel.data("prev",billing_country_sel.val());

    // Front end changes
    billing_country_sel.change(function () {
        var jqThis = $(this);
        var old_value = jqThis.data("prev");

        if(jqThis.val() != old_value) {
            rm_update_dialog(rm_gid, jqThis.value, old_value);
            //location.reload();
        }
        jqThis.data("prev",jqThis.val());
    });

    billing_prov_sel.change(function () {
        var jqThis = $(this);
        var old_value = jqThis.data("prev");
        if(billing_country_sel.val() == CRM.raic_members.country_code &&
            jqThis.val() != old_value) {
            rm_update_dialog(rm_gid, jqThis.value, old_value);
            //location.reload();
            jqThis.data("prev",jqThis.val());
        }

    });

    rm_cleanup_taxes_look();


    function rm_cleanup_taxes_look() {
        var custom_fields = CRM.raic_members.custom_fields;
        // Base price
        var base_amount = $('.editrow_custom_' + custom_fields.base_amount.field + '-section');
        var base_amount_text = base_amount.find('div.content').text();
        var base_amount_value = parseFloat(base_amount_text).toFixed(2);
        if(isFinite(base_amount_value)) {
          base_amount.find('div.content').text('$ ' + base_amount_value);
        }

        var i = 0;
        for(i = 0; i < custom_fields.taxes.length; i++) {

            var tax_rate = $('.editrow_custom_' + custom_fields.taxes[i].rate.field + '-section');
            var rate_text = tax_rate.find('div.content').text();
            tax_rate.find('div.content').text(parseFloat(rate_text).toFixed(3)+ '%');

            var tax_value = $('.editrow_custom_' + custom_fields.taxes[i].value.field + '-section');
            var value_text = tax_value.find('div.content').text();
            tax_value.find('div.content').text('$ ' + parseFloat(value_text).toFixed(2));

            if (tax_rate.find('div.label label').first().text() == 'NA Rate') {
                tax_rate.hide();
            }

            var value_text = tax_value.find('div.label label').first().text();
            if (value_text == 'NA Value' || value_text == 'NA Valeur') {
                tax_value.hide();
            }
        }


    }   // Clean up taxes appearance



    function rm_update_dialog( gid, new_value, old_value ) {
        var dataURL = "/civicrm/profile/edit?reset=1&snippet=5&context=dialog&blockNo=1&prefix=";
        dataURL = dataURL + '&gid=' + gid;
        $.ajax(dataURL)
          .done(function(content) {
                //alert( "success");
                var dia_div = $('#update-billing-dialog-1');
                dia_div.html(content);
                $("#Edit").submit(function() {
                    $(this).closest(".ui-dialog-content").dialog("close");
                });
                dia_div.show().dialog({
                    title: CRM.raic_members.billing_profile_title,
                    modal: true,
                    width: 650,
                    overlay: {
                        opacity: 0.5,
                        background: "black"
                    },
                    close: function( event, ui ) {
                        $('#_qf_Main_upload-bottom').attr("disabled", true);
                        location.reload();
                    }

                });

            }
          )
          .fail(function() {
            alert( "Please visit your user profile to update your billing address first.");
          });
    }

    // This function is not currently used. Not deleting just yet
    function _raic_members_get_province_name(province_id) {
        var provinces = [];
        provinces[1100] = "Alberta";
        provinces[1101] = "British Columbia";
        provinces[1102] = "Manitoba";
        provinces[1103] = "New Brunswick";
        provinces[1104] = "Newfoundland and Labrador";
        provinces[1105] = "Northwest Territories";
        provinces[1106] = "Nova Scotia";
        provinces[1107] = "Nunavut";
        provinces[1108] = "Ontario";
        provinces[1109] = "Prince Edward Island";
        provinces[1110] = "Quebec";
        provinces[1111] = "Saskatchewan";
        provinces[1112] = "Yukon Territory";
        return provinces[province_id];
    }
});


